This is a compilation of the 
- ETH Oberon/Aos/A2 System (named "A2" in the following), including Windows and Linux Emulations of the Active Object System and the Fox Compiler tool chain
and 
- the software packages Voyager and AntsInFields. 

Hopefully all parts of this compilation belong to the ETH A2 System

----

Oberon is the name of a modern integrated software environment.  It is a single-user, 
multi-tasking system that runs on bare hardware or on top of a host operating system. 
Oberon is also the name of a programming language in the Pascal/Modula tradition. 
The Oberon project was launched in 1985 by Niklaus Wirth and J�rg Gutknecht. 
See also http://www.oberon.ethz.ch

Voyager is a project to explore the feasibility of a portable and extensible system 
for simulation and data analysis systems. It is mainly written in and for Oberon. 
The Voyager project is carried out by StatLab Heidelberg and was launched 
in 1993 by G�nther Sawitzki. 
See also http://www.statlab.uni-heidelberg.de/projects/voyager/

AntsInFields is a Software Package for Simulation and Statistical Inference on Gibbs Fields.
AntsInFields is written in Oberon and uses Voyager. It has been developed since 1997
by Felix Friedrich. 

The A2 System is protected by the following copyright, start and end marked by ">>" and "<<" respectively:

>>
ETH Bluebottle/Aos/A2
Copyright (c) 2002-2016, Computer Systems Institute, ETH Zurich
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met: 

    * Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer. 
    * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution. 
    * Neither the name of the ETH Zurich nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission. 

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
<<

