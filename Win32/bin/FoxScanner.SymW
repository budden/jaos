MODULE FoxScanner;
	IMPORT Streams, Strings, Diagnostics, Basic := FoxBasic, D := Debugging, StringPool, ReaderSchitUTF8LF, UCS32, ПереводыЭлементовКода;
CONST 
	Trace = FALSE; 
	MaxIdentifierLength* = ПереводыЭлементовКода.МаксБуквВИдентификаторе; 
	MaxHexDigits* = 8; 
	MaxHugeHexDigits* = 16; 
	MaxRealExponent* = 38; 
	MaxLongrealExponent* = 308; 
	EOT* = 0X; 
	LF* = 0AX; 
	CR* = 0DX; 
	TAB* = 9X; 
	ESC* = 1BX; 
	None* = 0; 
	Equal* = 10; 
	DotEqual* = 20; 
	Unequal* = 30; 
	DotUnequal* = 40; 
	Less* = 50; 
	DotLess* = 60; 
	LessEqual* = 70; 
	DotLessEqual* = 80; 
	Greater* = 90; 
	DotGreater* = 100; 
	GreaterEqual* = 110; 
	DotGreaterEqual* = 120; 
	LessLessQ* = 130; 
	GreaterGreaterQ* = 140; 
	Questionmarks* = 150; 
	ExclamationMarks* = 160; 
	In* = 170; 
	Is* = 180; 
	Times* = 190; 
	TimesTimes* = 200; 
	DotTimes* = 210; 
	PlusTimes* = 220; 
	Slash* = 230; 
	Backslash* = 240; 
	DotSlash* = 250; 
	Div* = 260; 
	Mod* = 270; 
	Родись* = 278; 
	And0* = 279; 
	And1* = 280; 
	Or* = 290; 
	Plus* = 300; 
	Minus* = 310; 
	Not* = 320; 
	LeftParenthesis* = 330; 
	LeftBracket* = 340; 
	LeftBrace* = 350; 
	Number* = 360; 
	Character* = 370; 
	СтрокиЛитералМульти* = 380; 
	СтрокиЛитерал* = 390; 
	Nil* = 400; 
	Imag* = 410; 
	True* = 420; 
	False* = 430; 
	Self* = 440; 
	Result* = 450; 
	New* = 460; 
	Identifier* = 470; 
	If* = 480; 
	Case* = 490; 
	While* = 500; 
	Repeat* = 510; 
	For* = 520; 
	Loop* = 530; 
	With* = 540; 
	Exit* = 550; 
	Await* = 560; 
	Return* = 570; 
	Ignore* = 580; 
	Begin* = 590; 
	Semicolon* = 600; 
	Transpose* = 610; 
	RightBrace* = 620; 
	RightBracket* = 630; 
	RightParenthesis* = 640; 
	Questionmark* = 650; 
	ExclamationMark* = 660; 
	LessLess* = 670; 
	GreaterGreater* = 680; 
	Upto* = 690; 
	Arrow* = 700; 
	Period* = 710; 
	Comma* = 720; 
	Colon* = 730; 
	Of* = 740; 
	Then* = 750; 
	Do* = 760; 
	ToType2* = 770; 
	To* = 773; 
	ToLimit2* = 775; 
	By* = 780; 
	Becomes* = 790; 
	Bar* = 800; 
	End* = 810; 
	Else* = 820; 
	Elsif* = 830; 
	Until* = 840; 
	Finally* = 850; 
	Code* = 860; 
	Const* = 870; 
	Type* = 880; 
	Var* = 890; 
	Out* = 900; 
	Procedure* = 910; 
	Operator* = 920; 
	Import* = 930; 
	Definition* = 940; 
	Module* = 950; 
	Cell* = 960; 
	CellNet* = 970; 
	Extern* = 980; 
	Array* = 990; 
	Object* = 1000; 
	Record* = 1010; 
	Pointer* = 1020; 
	Enum* = 1030; 
	Port* = 1040; 
	Address* = 1050; 
	Size* = 1060; 
	Alias* = 1070; 
	Ln* = 1080; 
	PC* = 1090; 
	PCOffset* = 1100; 
	Shortint* = 1110; 
	Integer* = 1120; 
	Longint* = 1130; 
	Hugeint* = 1140; 
	Real* = 1150; 
	Longreal* = 1160; 
	Comment* = 1170; 
	ПереносСтроки* = 1180; 
	Отступ* = 1190; 
	ПробелыВнутриСтроки* = 1200; 
	НеразобранныйТекст* = 1205; 
	НеактивныйБлокПрепроцессора* = 1210; 
	ТекстПослеEnd* = 1215; 
	EndOfText* = 1220; 
	SingleQuote = 27X; 
	DoubleQuote* = 22X; 
	Ellipsis = 7FX; 
TYPE 
	StringType* = Strings.String; 

	IdentifierType* = StringPool.Index; 

	IdentifierString* = ПереводыЭлементовКода.СтрокаИдентификатора; 

	SubType* = SIGNED16; 

	ВариантыПеревода* = ПереводыЭлементовКода.ВариантыПеревода; 

	Keyword* = ARRAY 64 OF CHAR; 

	KeywordTable* = OBJECT (Basic.HashTableInt)
	VAR 
		table: POINTER TO ARRAY OF IdentifierType; 

		PROCEDURE ^  & InitTable*(size: SIZE); 
		PROCEDURE ^ SymbolByIdentifier*(identifier: IdentifierType): Symbol; 
		PROCEDURE ^ SymbolByString*(CONST name: ARRAY OF CHAR): Symbol; 
		PROCEDURE ^ IdentifierBySymbol*(symbol: Symbol; VAR identifier: IdentifierType); 
		PROCEDURE ^ StringBySymbol*(symbol: Symbol; VAR name: ARRAY OF CHAR); 
		PROCEDURE ^ PutString*(CONST name: ARRAY OF CHAR; symbol: Symbol); 
	END KeywordTable; 

	Symbol* = SIGNED32; 

	Position* = Basic.Position; 

	Token* = POINTER TO RECORD 
		position*: Position; 
		symbol-: Symbol; 
		identifier*: IdentifierType; 
		identifierString*: IdentifierString; 
		string*: StringType; 
		stringLength*: SIZE; 
		чемОткрываетсяСтрока*: UCS32.PStringJQ; 
		numberType*: SubType; 
		integer*: SIGNED64; 
		character*: CHAR; 
		real*: FLOAT64; 
		пред*: Token; 
		след*: Token; 
		неОтклПрепроц*: BOOLEAN; 
	END; 

	StringMaker* = OBJECT 
	VAR 
		length: SIZE; 
		data: StringType; 

		PROCEDURE ^  & Init*(initialSize: SIZE); 
		PROCEDURE ^ Add*(CONST buf: ARRAY OF CHAR; ofs, len: SIZE; propagate: BOOLEAN; VAR res: INTEGER); 
		PROCEDURE ^ Shorten*(n: SIZE); 
		PROCEDURE ^ Clear*; 
		PROCEDURE ^ GetWriter*(): Streams.Writer; 
		PROCEDURE ^ GetReader*(): Streams.Reader; 
		PROCEDURE ^ GetString*(VAR len: SIZE): StringType; 
		PROCEDURE ^ GetStringCopy*(VAR len: SIZE): StringType; 
	END StringMaker; 

	Scanner* = OBJECT 
	VAR 
		source-: StringType; 
		reader-: Streams.Reader; 
		diagnostics: Diagnostics.Diagnostics; 
		ch-: UCS32.CharJQ; 
		position-: Position; 
		havePeek: BOOLEAN; 
		peekCh: UCS32.CharJQ; 
		error-: BOOLEAN; 
		firstIdentifier: BOOLEAN; 
		case-: ВариантыПеревода; 
		stringWriter: Streams.Writer; 
		stringMaker: StringMaker; 
		useLineNumbers*: BOOLEAN; 
		предТокенВТЧПробел*: Token; 
		предТокенНеПробел*: Token; 

		PROCEDURE ^  & InitializeScanner*(CONST source: ARRAY OF CHAR; reader: Streams.Reader; pos: Position; diagnostics: Diagnostics.Diagnostics); 
		PROCEDURE ^ ResetCase*; 
		PROCEDURE ^ SetCase*(c: ВариантыПеревода); 
		PROCEDURE ^ ErrorS(CONST msg: ARRAY OF CHAR); 
		PROCEDURE ^ Error(code: SIGNED16); 
		PROCEDURE ^ ВозьмиСледБуквуНаСамомДеле(VAR куда: UCS32.CharJQ); 
		PROCEDURE ^ GetNextCharacter*; 
		PROCEDURE ^ Peek*(): UCS32.CharJQ; 
		PROCEDURE ^ ConsumeStringUntil(CONST endString: ARRAY OF UCS32.CharJQ; useControl: BOOLEAN); 
		PROCEDURE ^ GetEscapedString(VAR token: Token); 
		PROCEDURE ^ ПрочтиСтрокиЛитерал(VAR token: Token; multiLine, multiString, useControl: BOOLEAN; чемОткрывается: UCS32.PStringJQ); 
		PROCEDURE ^ GetIdentifier(VAR token: Token); 
		PROCEDURE ^ РазбериПробелы(VAR token: Token; sym: Symbol); 
		PROCEDURE ^ GetNumber(VAR token: Token): Symbol; 
		PROCEDURE ^ ПрочтиТекстПослеКонцаМодуля*(VAR token: Token); 
		PROCEDURE ^ ReadComment(VAR token: Token); 
		PROCEDURE ^ SkipToEndOfCode*(VAR startPos, endPos: Streams.Position; VAR token: Token): Symbol; 
		PROCEDURE ^ GetNextToken*(VAR token: Token): BOOLEAN; 
		PROCEDURE ^ GetNextTokenЗаметивСостояниеПрепроцессора*(VAR token: Token; неОтклПрепроц: BOOLEAN): BOOLEAN; 
		PROCEDURE ^ ДайТокенВТЧПробел*(VAR token: Token): BOOLEAN; 
		PROCEDURE ^ ДобавьЛексемуВЦепочку(лексема: Token); 
		PROCEDURE ^ ResetError*; 
		PROCEDURE ^ ResetErrorDiagnostics*(VAR diagnostics: Diagnostics.Diagnostics); 
	END Scanner; 

	Context* = RECORD 
		position: Position; 
		readerPosition: Streams.Position; 
		ch: CHAR; 
	END; 

	AssemblerScanner* = OBJECT (Scanner)
	VAR 
		startContext-: Context; 

		PROCEDURE ^  & InitAssemblerScanner*(CONST source: ARRAY OF CHAR; reader: Streams.Reader; position: Position; diagnostics: Diagnostics.Diagnostics); 
		PROCEDURE ^ GetContext*(VAR context: Context); 
		PROCEDURE ^ SetContext*(CONST context: Context); 
		PROCEDURE ^ SkipToEndOfLine*; 
		PROCEDURE ^ GetIdentifier(VAR token: Token); 
		PROCEDURE ^ GetNextToken*(VAR token: Token): BOOLEAN; 
	END AssemblerScanner; 
VAR 
	reservedCharacter: ARRAY UCS32.DiapazonJunikodaSKirillicejj OF BOOLEAN; 
	symbols-: ARRAY (EndOfText + 1) OF Keyword; 
	keywordsLower-, keywordsUpper-, keywords_Русские-: KeywordTable; 

	PROCEDURE ^ ORDв*(ч: ВариантыПеревода): UNSIGNED32; 
	PROCEDURE ^ ЗамениSymbolВЛексеме*(token: Token; symbol: Symbol); 
	PROCEDURE ^ NewScanner*(CONST source: ARRAY OF CHAR; reader: Streams.Reader; position: Streams.Position; diagnostics: Diagnostics.Diagnostics): Scanner; 
	PROCEDURE ^ NewAssemblerScanner*(CONST source: ARRAY OF CHAR; reader: Streams.Reader; position: Streams.Position; diagnostics: Diagnostics.Diagnostics): AssemblerScanner; 
	PROCEDURE ^ TokenToString*(CONST token: Token; case: ВариантыПеревода; VAR str: ARRAY OF CHAR); 
	PROCEDURE ^ НужноОтграничить_И_ОтСоседаЛи(сосед: Token): BOOLEAN; 
	PROCEDURE ^ TokenToString2*(CONST token: Token; case: ВариантыПеревода; VAR str: ARRAY OF CHAR); 
	PROCEDURE ^ TokenToString2a*(CONST token: Token; case: ВариантыПеревода; VAR str: ARRAY OF CHAR); 
	PROCEDURE ^ ПечЦепочкуЛексем*(п: Streams.Writer; CONST перваяЛексема: Token; printCase: ВариантыПеревода; info: BOOLEAN); 
	PROCEDURE ^ PrintToken*(w: Streams.Writer; CONST token: Token; case: ВариантыПеревода); 
	PROCEDURE ^ InitReservedCharacters; 
	PROCEDURE ^ GetKeyword*(case: ВариантыПеревода; symbol: Symbol; VAR identifier: IdentifierType); 
	PROCEDURE ^ InitSymbols; 
	PROCEDURE ^ InitKeywords; 
BEGIN
END FoxScanner.
