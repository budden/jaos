MODULE Trace;	(** Автор: "fn"

	Настраиваый системный протокол. 


 Примитивом записи является заменяемая процедура Char, остальные выражаются через неё. Реализации процедуры
 Char по соглашению называются TraceChar и могут меняться по ходу загрузки и в течение жизни исполняемого
 экземпляра. 
 

 Для этого лога основное требование - полнота вывода. Поэтому он не использует блокировки - некоторые клиенты
 могут не дождаться блокировки или сломаться, дожидаясь её. Соответственно, функции в нём должны быть
 неблокирующими и реентаребльными, до возможного предела. В реальности при выводе, например, в последовательный порт,
 происходит ожидание готовности порта, поэтому трассировка может приводить к приостановке выполнения. 
 
 Следствие из этого: при одновременных вызовах из двух активностей печати строк UTF-8 байты могут идти вперемежку, 
 предотвратить это в рамках требований к Char нельзя, и это фатально для записи UTF-8, поэтому нам придётся
 пока воспользоваться транслитом, а потом сделать примитивную пр-ру для вывода CharJQ.
 
 В Win32 и Linux32 вывод Trace при нашей обычной конфигурации происходит в консоль. 



*)

IMPORT SYSTEM, UCS32;

TYPE
	CharProc*= PROCEDURE (c:CHAR);
VAR
	Char*: CharProc;
	Color*: PROCEDURE (c: SIGNED8);

PROCEDURE Enter*;
BEGIN {UNCOOPERATIVE, UNCHECKED}
END Enter;

PROCEDURE Exit*;
BEGIN {UNCOOPERATIVE, UNCHECKED} Ln;
END Exit;

(** Send the specified characters to the trace output (cf. Streams.Sender). *)
PROCEDURE Send*(CONST buf: ARRAY OF CHAR; ofs, len: SIZE; propagate: BOOLEAN; VAR res: INTEGER);
BEGIN {UNCOOPERATIVE, UNCHECKED} INC (len, ofs); ASSERT (len <= LEN (buf)); WHILE ofs # len DO Char (buf[ofs]); INC (ofs); END; res := 0;
END Send;

(** Skip to the next line on trace output. *)
PROCEDURE Ln*;
BEGIN {UNCOOPERATIVE, UNCHECKED} Char (0DX); Char (0AX);
END Ln;

(** Write a string to the trace output. *)
PROCEDURE String* (CONST s: ARRAY OF CHAR);
VAR i: SIZE; c: CHAR;
BEGIN {UNCOOPERATIVE, UNCHECKED} FOR i := 0 TO LEN (s) - 1 DO c := s[i]; IF c = 0X THEN RETURN END; Char (c) END;
END String;




		(** Здесь беда: при параллельном выводе байты от разных источников будут идти вперемежку! 
		    Encode one UCS-32 value in ucs into one well-formed UTF-8 character. Sm takzhe UTF8Strings.EncodeChar *)
PROCEDURE UTF8Char*(ucs: UNSIGNED32);
VAR buf: ARRAY 7 OF CHAR; byte, i, mask, max: SIGNED16;
BEGIN
	IF (ucs <= 7FH) THEN
		Char(CHR(SHORT(ucs)));
		RETURN 
	ELSE
		byte := 5; mask := 7F80H; max := 3FH;
		WHILE (ucs > max) DO
			buf[byte] := CHR(80H + SHORT(ucs MOD 40H)); INC(byte, -1); (* CHR(80H + SHORT(AND(ucs, 3FH))) *)
			ucs := ucs DIV 64; (* LSH(ucs, -6) *)
			mask := mask DIV 2; (* 80H + LSH(mask, -1). Left-most bit remains set after DIV (mask is negative) *)
			max := max DIV 2 (* LSH(max, -1) *)
		END;
		buf[byte] := CHR(mask + SHORT(ucs)); 
		FOR i := 0 TO 5 - byte DO
			buf[i] := buf[i + byte] END;
		buf[6 - byte] := 0X;
		String(buf) END END UTF8Char;

		(** Encode one UCS-32 value in ucs into one well-formed UTF-8 character. Sm takzhe UTF8Strings.EncodeChar *)
PROCEDURE CharJQ*(ucs: UCS32.CharJQ);
BEGIN {UNCOOPERATIVE, UNCHECKED}
	UTF8Char(ucs.UCS32CharCode) END CharJQ;



(* НАЧАЛО Этот кусок есть в Streams, KernelLog и Trace, но в Trace отличаются флаги! *)

(** 
   Напечатай заканчивающуюся нулём строку UCS32 в формате UTF8.
	rezhim: 0 - просто выводит буквы
	1 - выводит буквы и завершает их нулём (как RawString)
	2 - выводит буквы в таких кавычках „“, закавычивая закрывающую кавычку, LF выводит как есть, Tab - как \t, прочие 
		управляющие буквы и буквы вне диапазона по кириллицу - в виде \NNN;
	3 - выводит буквы в таких кавычках \" "\, кавычку закавычивает, LF выводит как есть, Tab - как \t, 
		прочие управляющие буквы и буквы вне диапазона по кириллицу - в виде \NNN;  *)
PROCEDURE StringJQ*(CONST x : UCS32.StringJQ; rezhim : UNSIGNED8);
	VAR i: SIZE; kod : UNSIGNED32; 
BEGIN {UNCOOPERATIVE, UNCHECKED}
	i := 0;
	IF rezhim = 2 THEN UTF8Char(UCS32.DoubleLowDash9QuotationMark) 
	ELSIF rezhim = 3 THEN String('\"') 
	END;
	kod := x[i].UCS32CharCode;
	LOOP
		IF i = LEN(x) THEN
			UTF8Char(UCS32.ReplacementChar); EXIT END;
		kod := x[i].UCS32CharCode;
		IF kod = 0 THEN
			IF rezhim = 0 THEN
				Char(0X)
			ELSIF rezhim = 2 THEN
				UTF8Char(UCS32.LeftDoubleQuotationMark)
			ELSIF rezhim = 3 THEN
				String('"\') END;
			RETURN;
		ELSIF (rezhim = 2) & (kod = UCS32.LeftDoubleQuotationMark) THEN
			Char("\"); UTF8Char(kod)
		ELSIF (rezhim = 3) & (kod = ORD('"')) THEN
			Char("\"); UTF8Char(kod)
		ELSIF (rezhim >= 2) & ((kod < ORD(" ")) OR (kod >= UCS32.DiapazonJunikodaSKirillicejj)) THEN
			IF kod = UCS32.KodTAB THEN
				String("\t")
			ELSIF (kod = UCS32.KodLF) 
				OR (kod = UCS32.LeftDoubleQuotationMark)
				OR (kod = UCS32.DoubleLowDash9QuotationMark) THEN
				UTF8Char(kod)
			ELSE
				Char("\");
				Hex(kod, 0);
				Char(";") END
		ELSE
			UTF8Char(kod) END;
		INC(i) END END StringJQ;
	
PROCEDURE UCS32StringJQLiteral*(CONST x : UCS32.StringJQ);
BEGIN {UNCOOPERATIVE, UNCHECKED} StringJQ(x, 1) END UCS32StringJQLiteral;

PROCEDURE UCS32StringJQvUTF8*(CONST x : UCS32.StringJQ);
BEGIN {UNCOOPERATIVE, UNCHECKED} StringJQ(x, 0) END UCS32StringJQvUTF8;


(* КОНЕЦ Этот кусок есть в Streams, KernelLog и Trace *)


(** Write a string to the trace output and skip to next line. *)
PROCEDURE StringLn* (CONST s: ARRAY OF CHAR);
BEGIN {UNCOOPERATIVE, UNCHECKED} String (s); Ln;
END StringLn;

(** Write a character. *)
PROCEDURE Int* (x: SIGNED64; w: SIZE);
VAR i: SIZE; x0: SIGNED64; a: ARRAY 21 OF CHAR;
BEGIN {UNCOOPERATIVE, UNCHECKED}
	IF x < 0 THEN
		IF x = MIN (SIGNED64) THEN
			DEC (w, 20);
			WHILE w > 0 DO Char (' '); DEC (w) END;
			String ("-9223372036854775808");
			RETURN
		ELSE
			DEC (w); x0 := -x
		END
	ELSE
		x0 := x
	END;
	i := 0;
	REPEAT
		a[i] := CHR (x0 MOD 10 + 30H); x0 := x0 DIV 10; INC (i)
	UNTIL x0 = 0;
	WHILE w > i DO Char (' '); DEC (w) END;
	IF x < 0 THEN Char ('-') END;
	REPEAT DEC (i); Char (a[i]) UNTIL i = 0
END Int;

PROCEDURE Boolean* (x : BOOLEAN);
BEGIN {UNCOOPERATIVE, UNCHECKED} IF x THEN String ("TRUE") ELSE String ("FALSE") END
END Boolean;

(** Write "x" as a decimal number with a power-of-two multiplier (K, M or G), followed by "suffix". "w" is the field width, excluding "suffix". *)
PROCEDURE IntSuffix* (x: SIGNED64; w: SIZE; CONST suffix: ARRAY OF CHAR);
CONST K = 1024; M = K*K; G = K*M;
VAR mult: CHAR;
BEGIN {UNCOOPERATIVE, UNCHECKED}
	IF x MOD K # 0 THEN
		Int (x, w)
	ELSE
		IF x MOD M # 0 THEN mult := 'K'; x := x DIV K
		ELSIF x MOD G # 0 THEN mult := 'M'; x := x DIV M
		ELSE mult := 'G'; x := x DIV G
		END;
		Int (x, w-1); Char (mult)
	END;
	String (suffix)
END IntSuffix;

(**
	Write an integer in hexadecimal right-justified in a field of at least ABS(w) characters.
	If w < 0 THEN w least significant hex digits of x are written (possibly including leading zeros)
*)
PROCEDURE Hex*(x: SIGNED64; w: SIZE );
VAR i: SIZE;
	buf: ARRAY 2*SIZEOF(SIGNED64)+2 OF CHAR;
	neg: BOOLEAN;
	c: SIGNED64;
BEGIN {UNCOOPERATIVE, UNCHECKED}
	IF w >= 0 THEN
		i:= 0;
		IF x < 0 THEN neg := TRUE; x :=-x ELSIF x=0 THEN buf := "0" ELSE neg := FALSE END;
		i := 0;
		REPEAT
			c := x MOD 10H;
			IF c < 10 THEN buf[i] := CHR(c+ORD("0")) ELSE buf[i] := CHR(c-10+ORD("A")) END;
			x := x DIV 10H;
			INC(i);
		UNTIL (i = 2 * SIZEOF(SIGNED64)) OR (x=0);
		IF c > 9 THEN buf[i] := "0"; INC(i) END;
		IF neg THEN buf[i] := "-"; INC(i) END;
		WHILE(w > i) DO Char(" "); DEC(w); END;
		REPEAT DEC(i); Char(buf[i]); UNTIL i=0;
	ELSE
		w := -w;
		WHILE(w>2*SIZEOF(SIGNED64)) DO
			Char(" "); DEC(w);
		END;
		buf[w] := 0X;
		REPEAT
			DEC(w);
			c := x MOD 10H;
			IF c <10 THEN buf[w] := CHR(c+ORD("0")) ELSE buf[w] := CHR(c-10+ORD("A")) END;
			x := x DIV 10H;
		UNTIL w = 0;
		String(buf);
	END;

END Hex;

(** Write "x" as a hexadecimal address *)
PROCEDURE Address* (x: ADDRESS);
BEGIN {UNCOOPERATIVE, UNCHECKED}
	Hex(x,-2*SIZEOF(ADDRESS));
END Address;

(** Write "x" as a size *)
PROCEDURE Size* (x: SIZE);
BEGIN {UNCOOPERATIVE, UNCHECKED}
	Int(x,0);
END Size;

(** Write "x" as a hexadecimal number. "w" is the field width. Always prints 16 digits. *)
PROCEDURE HIntHex* (x: SIGNED64; w: SIZE);
BEGIN {UNCOOPERATIVE, UNCHECKED} Hex (x, w);
END HIntHex;

(** Write a block of memory in hex. *)
PROCEDURE Memory* (adr: ADDRESS; size: SIZE);
VAR i, j: ADDRESS; ch: CHAR;
BEGIN {UNCOOPERATIVE, UNCHECKED}
	size := adr+size-1;
	FOR i := adr TO size BY 16 DO
		Address (i); Char (' ');
		FOR j := i TO i+15 DO
			IF j <= size THEN
				SYSTEM.GET (j, ch);
				Char(' ');
				Hex (ORD (ch), -2)
			ELSE
				Char (' ');
				Char (' ');
				Char (' ');
			END
		END;
		Char (' '); Char (' ');
		FOR j := i TO i+15 DO
			IF j <= size THEN
				SYSTEM.GET (j, ch);
				IF (ch < ' ') OR (ch >= CHR (127)) THEN ch := '.' END;
				Char (ch)
			END
		END;
		Ln
	END;
END Memory;

(* Write basic stack frame information *)
PROCEDURE StackFrames- (skip, count, size: SIZE);
VAR frame {UNTRACED}: POINTER {UNSAFE} TO RECORD descriptor, previous, caller, parameters: ADDRESS END;
BEGIN {UNCOOPERATIVE, UNCHECKED}
	frame := ADDRESS OF frame + SIZE OF ADDRESS;
	WHILE skip # 0 DO
		frame := frame.previous;
		IF frame = NIL THEN RETURN END;
		DEC (skip);
	END;
	WHILE count # 0 DO
		Address (frame.caller); Char (':'); Ln;
		IF frame.previous = NIL THEN RETURN END;
		IF frame.previous - ADDRESS OF frame.descriptor <= size THEN
			Memory (ADDRESS OF frame.parameters, frame.previous - ADDRESS OF frame.descriptor);
		ELSIF size # 0 THEN
			Memory (ADDRESS OF frame.parameters, size); String ("..."); Ln;
		END;
		DEC (count); frame := frame.previous;
	END;
	WHILE frame.previous # NIL DO INC (count); frame := frame.previous END;
	IF count # 0 THEN Char ('+'); Int (count, 0); Ln; END;
END StackFrames;

(** Write a buffer in hex. *)
PROCEDURE Buffer* (CONST buf: ARRAY OF CHAR; ofs, len: SIZE);
BEGIN {UNCOOPERATIVE, UNCHECKED} Memory (ADDRESSOF (buf[ofs]), len)
END Buffer;

(** Write bits (ofs..ofs+n-1) of x in binary. *)
PROCEDURE Bits* (x: SET; ofs, n: SIZE);
BEGIN {UNCOOPERATIVE, UNCHECKED}
	REPEAT
		DEC (n);
		IF (ofs+n) IN x THEN Char ('1') ELSE Char ('0') END
	UNTIL n = 0
END Bits;

PROCEDURE Set*(x: SET);
VAR first: BOOLEAN; i: SIZE;
BEGIN
	first := TRUE;
	Char("{");
	FOR i := 0 TO MAX(SET) DO
		IF i IN x THEN
			IF ~first THEN Char(",") ELSE first := FALSE END;
			Int(i,1);
		END;
	END;
	Char("}");
END Set;


(** Colors *)
PROCEDURE Blue*;
BEGIN {UNCOOPERATIVE, UNCHECKED} Color (9);
END Blue;

PROCEDURE Green*;
BEGIN {UNCOOPERATIVE, UNCHECKED} Color (10);
END Green;

PROCEDURE Red*;
BEGIN {UNCOOPERATIVE, UNCHECKED} Color (12);
END Red;

PROCEDURE Yellow*;
BEGIN {UNCOOPERATIVE, UNCHECKED} Color (14);
END Yellow;

PROCEDURE Default*;
BEGIN {UNCOOPERATIVE, UNCHECKED} Color (7);
END Default;

PROCEDURE NullChar(c: CHAR);
BEGIN {UNCOOPERATIVE, UNCHECKED}
END NullChar;

PROCEDURE NullColor(c: SIGNED8);
BEGIN {UNCOOPERATIVE, UNCHECKED}
END NullColor;

PROCEDURE Init*;
BEGIN {UNCOOPERATIVE, UNCHECKED}
	Char := NullChar;
	Color := NullColor;
END Init;


(* NapechatajjKodyBukvStringJQ - отладочная утилита, к-рую можно использовать, если непонятно, что же находится в нашей строке. 
   Она же открывает нам знание о том, что строковый литерал в виде текста - это не идеальная форма представления строкового 
   литерала, поскольку он не читается с экрана. Не факт, что ей место именно здесь.
   
   См. также NapechatajjKodyBukvArrayOfChar *)
PROCEDURE NapechatajjKodyBukvStringJQ*(CONST x : UCS32.StringJQ; zaKoncomTozhe : BOOLEAN); 
VAR i : SIZE;
BEGIN
	LOOP 
		IF i = LEN(x) - 1 THEN 
			Ln; EXIT END;
		Int(i,0); String("="); Int(x[i].UCS32CharCode,0); 
		IF i MOD 16 = 15 THEN 
			Ln
		ELSE
			String(" ") END;
		IF (x[i].UCS32CharCode = 0) & ~zaKoncomTozhe THEN 
			Ln; EXIT END; 
		INC(i) END END NapechatajjKodyBukvStringJQ;


(* NapechatajjKodyBukvArrayOfChar - печатает коды знаков в ARRAY OF CHAR. *)
PROCEDURE NapechatajjKodyBukvArrayOfChar*(CONST x : ARRAY OF CHAR; zaKoncomTozhe : BOOLEAN); 
VAR i : SIZE;
BEGIN
	LOOP 
		IF i = LEN(x) - 1 THEN 
			Ln; EXIT END;
		Int(i,0); String("="); Int(ORD(x[i]),0); 
		IF i MOD 16 = 15 THEN 
			Ln
		ELSE
			String(" ") END;
		IF (ORD(x[i]) = 0) & ~zaKoncomTozhe THEN 
			Ln; EXIT END; 
		INC(i) END END NapechatajjKodyBukvArrayOfChar;

(*
BEGIN
	Char := NullChar;
	Color := NullColor;
	*)
END Trace.

