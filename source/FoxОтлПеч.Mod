MODULE FoxОтлПеч; (** Отладочная печать деревьев синт. разбора Fox. (С) Денис Будяк 2020 *)

IMPORT
	Scanner := FoxScanner, SyntaxTree := FoxSyntaxTree, 
	Basic := FoxBasic, Streams,
	SYSTEM, 
	pp := PodrobnajaPechatq,  KernelLog, 
	УтилитыДляПереводаКода := FoxУтилитыДляПереводаКода, Modules
	;

(* Устанавливает пр-ру отладочной печати в такую, которая умеет 
   печатать подробности синтаксических деревьев *)
PROCEDURE ПечататьСинтДеревьяОсобо*;
BEGIN
	pp.процедураОтладочнойПечатиПоУмолчанию := МояОтлПеч;
END ПечататьСинтДеревьяОсобо;

PROCEDURE НапечатайПоследнийРазобранныйМодуль*;
VAR kl : Streams.Writer; k : pp.KontekstPechati;
BEGIN
	Streams.OpenWriter(kl, KernelLog.Send);
	(* pp.процедураОтладочнойПечатиПоУмолчанию := NIL; *)
	pp.ZadajjKontekstPechatiPoUmolch(k); k.w := kl;
	k.процедураОтладочнойПечати := МояОтлПеч;
	k.пределГлубины := 9;
	pp.PechVKontekste(k,SyntaxTree.последнийРазобранныйМодуль,FALSE);
	END НапечатайПоследнийРазобранныйМодуль;

PROCEDURE Печ*(w : Streams.Writer; чё: ANY);	
VAR k : pp.KontekstPechati;
BEGIN
	pp.ZadajjKontekstPechatiPoUmolch(k); k.w := w;
	k.процедураОтладочнойПечати := МояОтлПеч;
	pp.PechVKontekste(k, чё, FALSE);
END Печ;

(*	Возврат: 0 - процедура не умеет печатать такую запись, 
		1 - напечатано, 
		2 - ошибка при печати (при получении этого возврата процедура
		отладочной печати будет сброшена в пустую)
*)
PROCEDURE МояОтлПеч(VAR k: pp.KontekstPechati; CONST оит: pp.ОбъектИТип; CONST имяТипа: pp.ModulesName) : UNSIGNED8;
VAR имяМодуля: pp.ModulesName; w: Streams.Writer; адр : ADDRESS;
BEGIN 
	w := k.w;
	IF оит.mod = NIL THEN RETURN 0 END;
	COPY(оит.mod.name,имяМодуля);
	IF имяМодуля # "FoxSyntaxTree" THEN RETURN 0 END;
	адр := pp.АдресИзОбъектИТип(оит);
	IF имяТипа = "Identifier" THEN
		RETURN ПечIdentifier(k,SYSTEM.VAL(SyntaxTree.Identifier, адр));
	ELSIF имяТипа = "Module" THEN
		RETURN ПечModule(k,SYSTEM.VAL(SyntaxTree.Module, адр));
	ELSIF имяТипа = "ModuleScope" THEN
		RETURN ПечModuleScope(k, SYSTEM.VAL(SyntaxTree.ModuleScope, адр));
	ELSIF имяТипа = "Import" THEN
		RETURN ПечImport(k, SYSTEM.VAL(SyntaxTree.Import, адр));
	ELSIF имяТипа = "Procedure" THEN
		RETURN ПечProcedure(k, SYSTEM.VAL(SyntaxTree.Procedure, адр));
	ELSIF имяТипа = "SymbolDesignator" THEN
		RETURN ПечSymbolDesignator(k, SYSTEM.VAL(SyntaxTree.SymbolDesignator, адр));
	ELSIF имяТипа = "ParameterDesignator" THEN
		RETURN ПечParameterDesignator(k, SYSTEM.VAL(SyntaxTree.ParameterDesignator, адр));
	ELSE
		RETURN 0 END END МояОтлПеч;
		
PROCEDURE ПечImport(VAR k: pp.KontekstPechati; im: SyntaxTree.Import) : UNSIGNED8;
VAR рез: UNSIGNED8;
BEGIN
	IF (im = NIL)OR(k.ЗапасГлубины()<1) THEN
		ПечСсылSymbol(k, im); RETURN 1 END;
	ПечСсылSymbol(k, im); 
	(* k.w.String(" moduleName:"); ПечIdentifier(k, im.moduleName); *)
	IF im.direct THEN k.w.String("(direct)") END;
	INC(k.текГлубина, 1); k.w.Ln; k.Отступ(0);
	k.w.String(" module:");
	рез :=  ПечModule(k,im.module); 
	INC(k.текГлубина, -1); RETURN рез END ПечImport;
	
PROCEDURE ПечProcedure(VAR k: pp.KontekstPechati; p: SyntaxTree.Procedure) : UNSIGNED8;
BEGIN
	IF (p = NIL)OR(k.ЗапасГлубины()<1) THEN
		ПечСсылSymbol(k, p); RETURN 1 END;
	ПечСсылSymbol(k, p); 
	INC(k.текГлубина, 1); 
	k.w.Ln; k.Отступ(0); k.w.String("level:"); k.w.Int(p.level,0);
	k.w.Ln; k.Отступ(0); k.w.String("procedureScope:"); 
	pp.ПечВКонтекстеВнутр(k, p.procedureScope, FALSE, TRUE);
	IF p.procedureScope # NIL THEN
		k.w.Ln; k.Отступ(0); k.w.String("procedureScope.body:") END;
		pp.ПечВКонтекстеВнутр(k, p.procedureScope.body, FALSE, TRUE);
	INC(k.текГлубина, -1); 
	RETURN 1 END ПечProcedure;



PROCEDURE ПечСсылModule(VAR k: pp.KontekstPechati; m: SyntaxTree.Module);
BEGIN
	IF m = NIL THEN k.w.String("«No Module»") END;
	pp.ПечВКонтекстеВнутр(k, m, FALSE, TRUE); 
	k.w.String("«Module ");
	ПечBasicString(k,m.name.и);
	k.w.String("»") END ПечСсылModule;

PROCEDURE ПечModule(VAR k: pp.KontekstPechati; m: SyntaxTree.Module) : UNSIGNED8;
BEGIN
	IF (m = NIL)OR(k.ЗапасГлубины()<1) THEN
		ПечСсылModule(k, m); RETURN 1 END;
	ПечСсылSymbol(k, m);  
	k.w.String(" sourceName: "); k.w.String(m.sourceName); 
	k.w.String(" context: "); IGNORE ПечIdentifier(k, m.context); 
	INC(k.текГлубина, 1); k.w.Ln; k.Отступ(0);
	pp.ПечВКонтекстеВнутр(k, m.moduleScope, FALSE, FALSE); 
	INC(k.текГлубина, -1);
	k.w.Update;
	RETURN 1 END ПечModule;
	
	
PROCEDURE ПечСсылSymbol(VAR k: pp.KontekstPechati; s: SyntaxTree.Symbol);
BEGIN
	pp.ПечВКонтекстеВнутр(k, s, FALSE, TRUE); 
	k.w.String("«");
	k.w.Update;
	k.w.String(Modules.TypeOf(s).name);
	k.w.String("(Symbol) "); IGNORE ПечIdentifier(k, s.name); k.w.String(" ");
	IF (s.name.л = NIL) OR (s.name.л.position.start # s.position.start) THEN
		k.w.String(" position.start:"); k.w.Int(s.position.start,0); END;
	k.w.String("»");
END ПечСсылSymbol;


PROCEDURE ПечSymbolDesignator(VAR k: pp.KontekstPechati; s: SyntaxTree.SymbolDesignator): UNSIGNED8;
BEGIN
	pp.ПечВКонтекстеВнутр(k, s, FALSE, TRUE); 
	k.w.String("«");
	k.w.Update;
	k.w.String(Modules.TypeOf(s).name);
	k.w.String("(SymbolDesignator) "); IGNORE ПечIdentifier(k, s.symbol.name); k.w.String(" ");
	IF (s.symbol.name.л = NIL) OR (s.symbol.name.л.position.start # s.position.start) THEN
		k.w.String(" position.start:"); k.w.Int(s.position.start,0); END;
	k.w.String("»");
	RETURN 1;
END ПечSymbolDesignator;


PROCEDURE ПечParameterDesignator(VAR k: pp.KontekstPechati; s: SyntaxTree.ParameterDesignator): UNSIGNED8;
BEGIN
	pp.ПечВКонтекстеВнутр(k, s, FALSE, TRUE); 
	k.w.String("«");
	k.w.Update;
	k.w.String(Modules.TypeOf(s).name);
	k.w.String("(ParameterDesignator) left:"); pp.ПечВКонтекстеВнутр(k, s.left, FALSE, FALSE); k.w.String(" ");
	k.w.String(" param1:"); pp.ПечВКонтекстеВнутр(k, s.parameters, FALSE, FALSE);
	k.w.String("»");
	RETURN 1;
END ПечParameterDesignator;


(* (* Эта процедура работала, когда Identifier был записью,
   а не указателем. Она показывает пример, как это делается *)
TYPE PIdentifier = POINTER {UNSAFE, UNTRACED} TO SyntaxTree.Identifier; 
PROCEDURE ПечIdentifierПоАдр(VAR k: pp.KontekstPechati; адр: ADDRESS) : UNSIGNED8;
	VAR ч: PIdentifier; чч : SyntaxTree.Identifier;
BEGIN
	ч := SYSTEM.VAL(PIdentifier, адр);
	чч := ч^;
	ПечIdentifier(k, чч); RETURN 1 END ПечIdentifierПоАдр; *)
	

PROCEDURE ПечBasicString(VAR k: pp.KontekstPechati; bs: Basic.String);
	VAR ids: Scanner.IdentifierString; 
BEGIN
	IF bs = Basic.invalidString THEN
		k.w.String("«No String»")
	ELSE
		Basic.GetString(bs,ids);
		k.w.String(ids) END END ПечBasicString;

PROCEDURE ПечIdentifier(VAR k: pp.KontekstPechati; CONST ч: SyntaxTree.Identifier): UNSIGNED8;
BEGIN
	k.w.String("≡");
	ПечBasicString(k,ч.и);
	IF (ч.п # Basic.emptyString) THEN
		k.w.String("|");
		ПечBasicString(k,ч.п) END;
	IF (ч.л # NIL) THEN
		k.w.String("@");
		k.w.Int(ч.л.position.start,0) END;
	k.w.String(" ");
	RETURN 1;
END ПечIdentifier;


PROCEDURE ПечModuleScope(VAR k: pp.KontekstPechati; s: SyntaxTree.ModuleScope): UNSIGNED8;
BEGIN
	k.w.String("«ModuleScope ");
	ПечScope(k,s); k.w.String("»"); RETURN 1 END ПечModuleScope;

PROCEDURE ПечСсылScope(VAR k: pp.KontekstPechati; s: SyntaxTree.Scope);
VAR w: Streams.Writer;
BEGIN
	w := k.w;
	w.String("«Scope: ");
	IF s.firstSymbol = NIL THEN
		w.String("«no symbols»")
	ELSE
		ПечСсылSymbol(k, s.firstSymbol);
		w.String("("); w.Int(s.numberSymbols,0); w.String(")") END;
	IF s.пэкИменованнаяОбластьВидимости = NIL THEN
		w.String("«НУЛЬ переводов»")
	ELSIF s.пэкИменованнаяОбластьВидимости = УтилитыДляПереводаКода.нулеваяОбластьВидимости THEN
		w.String("«нет переводов»")
	ELSE
		w.String("«1-й перевод: "); w.String(s.пэкИменованнаяОбластьВидимости.ru^); w.String("»") END;
	IF s.outerScope # NIL THEN
		w.String("\");
		ПечСсылScope(k,s.outerScope);
	ELSIF s.ownerModule # NIL THEN
		w.String("\");
		ПечСсылSymbol(k, s.ownerModule)
	END;
	w.String("»"); w.Update;
END ПечСсылScope;



PROCEDURE ПечScope(VAR k: pp.KontekstPechati; s: SyntaxTree.Scope);
	VAR сим: SyntaxTree.Symbol;
BEGIN
	IF (s = NIL)OR(k.ЗапасГлубины() < 1) THEN
		ПечСсылScope(k, s); RETURN END;
	k.w.String("«Scope"); INC(k.текГлубина);
	k.w.Ln; k.Отступ(0);
	IF s.ownerModule # NIL THEN
		k.w.String("ownerModule:"); ПечСсылModule(k, s.ownerModule) END;
	k.w.Ln; k.Отступ(0);
	k.w.String("firstSymbol(и следующие): ["); INC(k.текГлубина);
	сим := s.firstSymbol;
	WHILE сим # NIL DO
		k.w.Ln; k.Отступ(0);
		ПечСсылSymbol(k, сим);
		сим := сим.nextSymbol END;
	INC(k.текГлубина, -2);		
	k.w.String("] /Scope»"); 
	END ПечScope;
	

(*			PROCEDURE debugWriteScope(w: Streams.Writer; s: SyntaxTree.Scope);
		BEGIN
			w.String("Переделай debugWriteScope на FoxОтлПеч");
			w.String("«Scope: ");
			IF s.firstSymbol = NIL THEN
				w.String("«no symbols»")
			ELSE
				debugWriteSymbol(w, s.firstSymbol);
				w.String("("); w.Int(s.numberSymbols,0); w.String(")") END;
			IF s.пэкИменованнаяОбластьВидимости = NIL THEN
				w.String("«НУЛЬ переводов»")
			ELSIF s.пэкИменованнаяОбластьВидимости = УтилитыДляПереводаКода.нулеваяОбластьВидимости THEN
				w.String("«нет переводов»")
			ELSE
				w.String("«1-й перевод: "); w.String(s.пэкИменованнаяОбластьВидимости.ru^); w.String("»") END;
			IF s.outerScope # NIL THEN
				w.String("\");
				debugWriteScope(w,s.outerScope);
			ELSIF
				s.ownerModule # NIL THEN
				w.String("\");
				debugWriteSymbol(w, s.ownerModule)
			END;
			w.String("»"); w.Update;
		END debugWriteScope;
 *)




BEGIN
END FoxОтлПеч.

System.DoCommands
	FoxОтлПеч.Дуй~
	WMKernelLog.Open~~
