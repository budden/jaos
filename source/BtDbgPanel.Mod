MODULE BtDbgPanel; (** AUTHOR ""; PURPOSE ""; *)

IMPORT 
	SYSTEM,
	KernelLog, WMComponents, 
	Modules,WM := WMWindowManager, 
	WMGraphics, WMRectangles, Strings, Streams,
	WMProperties,
	WMStandardComponents,
	WMTrees,
	Kernel32,(*Only for get type Kernel32.Context*)
	(* Machine , Heaps, *)
	WMMessages,
	WMGrids, WMStringGrids, BtDecoder,
	Inputs
;
(*Common types and constants*)
TYPE
(* String=Strings.String; *)
Canvas = WMGraphics.Canvas;

CONST
	AlignTop=WMComponents.AlignTop;
	(* AlignNone=WMComponents.AlignNone; *)
	(* AlignLeft=WMComponents.AlignLeft; *)
	AlignClient=WMComponents.AlignClient;
	(* AlignRight=WMComponents.AlignRight;
	AlignBottom=WMComponents.AlignBottom;
	DataLen = 1024; *)

	mouseWheelScrollSpeedI = 3;
TYPE


	VariablesPanel* = OBJECT(WMComponents.VisualComponent)
	VAR
		(* owner : WMComponents.FormWindow; *)
		
		trv*:WMTrees.TreeView;
		nodeSelected:ANY;

		PROCEDURE &Init*;
		VAR
			(* i:SIGNED32;
			font:WMGraphics.Font; *)
			tpnl : WMStandardComponents.Panel; 
		BEGIN
			Init^;
			SetNameAsString(Strings.NewString("VariablesPanel"));
			NEW (tpnl); tpnl.bounds.SetHeight (16); tpnl.alignment.Set(AlignTop); 
			
			
			nodeSelected:=NIL;
			NEW(trv); trv.alignment.Set(AlignClient); 
			trv.bounds.SetWidth(300);
			trv.onSelectNode.Add(NodeSelected);
(*			trv.SetDrawNodeProc(DrawNode);*)
			AddInternalComponent(trv);
			
		END Init;
		
		PROCEDURE SetNewRoot*(r:WMTrees.TreeNode);
		BEGIN
			
		END SetNewRoot;
		
		(* PROCEDURE DrawNode(canvas: WMGraphics.Canvas; w, h: SIGNED32; node: WMTrees.TreeNode; state: SET);
		VAR dx, tdx, tdy : SIGNED32; f : WMGraphics.Font; image : WMGraphics.Image; caption: String;
			val:ARRAY 15 OF CHAR; 
		BEGIN
		END DrawNode; *)

		PROCEDURE NodeSelected(sender, data : ANY);
		(* VAR t : WMTrees.Tree; 
			node : WMTrees.TreeNode; *)
		BEGIN
			IF (data # NIL) & (data IS WMTrees.TreeNode) THEN
				nodeSelected:=data;
			END;
		END NodeSelected;
		
		(* VariablesPanel *)
		PROCEDURE Handle(VAR m: WMMessages.Message);
		BEGIN
			(* KernelLog.String("VariablesPanel.Handle"); KernelLog.Ln; *)
			IF m.msgType = WMMessages.MsgKey THEN
				(* KernelLog.Hex(m.y,0); KernelLog.Ln; *)
				IF ~HandleVariablesPanelShortcut(m.x, m.flags, m.y) THEN
					Handle^(m);
				END;
			ELSE Handle^(m)
			END
		END Handle;
		
		(* Iz TFPET.Mod *)
		PROCEDURE ControlKeyDown(flags : SET) : BOOLEAN;
		BEGIN	RETURN (flags * Inputs.Ctrl # {}) & (flags - Inputs.Ctrl = {}) END ControlKeyDown;

	
		PROCEDURE HandleVariablesPanelShortcut(ucs : SIGNED32; flags : SET; keysym : SIGNED32) : BOOLEAN;
		BEGIN
		IF (keysym = 10H) & ControlKeyDown(flags) THEN (* CTRL-P *)
			KernelLog.String("Control-P!"); KernelLog.Ln;
			(* FIXME vyzvatq Inspektirujj, k-ryjj sejjchas zhivetq v WMDebugger *)
		ELSE
			RETURN FALSE END; 
		RETURN TRUE END HandleVariablesPanelShortcut;
		

	
	END VariablesPanel;

VAR
 flOfsArr : FlOfsArr;
 flIdArr: FlStrArr;
 flDescArr : FlStrArr;
CONST
 FlCount = 15;
TYPE
	FlOfsArr = ARRAY FlCount OF SIGNED32;
	FlStrArr = ARRAY FlCount OF Strings.String;
	RegPanel* = OBJECT(WMStandardComponents.Panel)
	VAR
		cur-,old-:Kernel32.Context;
		font,fontb:WMGraphics.Font;

		PROCEDURE &Init;
		BEGIN
			Init^;
			SetNameAsString(Strings.NewString("RegPanel"));
			fillColor.Set(SIGNED32(0FFFFFFFFH));

			font:=WMGraphics.GetFont("Courier", 12, {});
			fontb:=WMGraphics.GetFont("Courier", 12, {0});
				(* -- Editor Area *)
		END Init;
		PROCEDURE UpDate*(cur:Kernel32.Context);
		BEGIN
			old:=SELF.cur;
			SELF.cur:=cur;
			Invalidate;
		END UpDate;

		PROCEDURE DrawBackground(canvas : WMGraphics.Canvas);
		VAR
			i,l,y,dy:SIGNED32;

		PROCEDURE DrwReg(s:ARRAY OF CHAR;v1,v2:SIGNED32);
		VAR
			str:ARRAY 9 OF CHAR;
		BEGIN
			canvas.SetColor(WMGraphics.Black);
			canvas.SetFont(fontb);
			canvas.DrawString(l,y,s);
			canvas.SetFont(font);
			IF v1#v2 THEN canvas.SetColor(WMGraphics.Red) END;
			Strings.IntToHexStr(v1, 8, str);
			canvas.DrawString(l+9*3,y,str);
			INC(y,dy);
		END DrwReg;

		PROCEDURE DrwFlag(i:SIGNED32);
		VAR
			(* str:ARRAY 9 OF CHAR; *)
			c,o:SET;
			cb,ob:BOOLEAN;
		BEGIN
			c:=SYSTEM.VAL(SET,cur.FLAGS);
			o:=SYSTEM.VAL(SET,old.FLAGS);
			canvas.SetColor(WMGraphics.Black);
			canvas.SetFont(fontb);
			canvas.DrawString(l,y,flIdArr[i]^);
			canvas.SetFont(font);
			canvas.DrawString(l+35,y,flDescArr[i]^);
			cb:=flOfsArr[i] IN c;
			ob:=flOfsArr[i] IN o;
			IF  cb#ob THEN canvas.SetColor(WMGraphics.Red) END;
			IF cb THEN
				canvas.DrawString(l+25,y,"1");
			ELSE
				canvas.DrawString(l+25,y,"0");
			END;
			INC(y,dy);
		END DrwFlag;
		
		BEGIN
			DrawBackground^(canvas);
			canvas.SetFont(font);
			y:=15;
			l:=5;
			dy:=15;
				DrwReg("RA",cur.RA ,old.RA );DrwReg("RB",cur.RB ,old.RB );DrwReg("RC",cur.RC ,old.RC );DrwReg("RD",cur.RD ,old.RD );
				INC(y,7);
				DrwReg("RSI",cur.RSI ,old.RSI ); DrwReg("RDI",cur.RDI ,old.RDI );
				INC(y,7);
				DrwReg("EBP",cur.BP ,old.BP ); DrwReg("ESP",cur.SP ,old.SP );
				INC(y,7);
				DrwReg("EIP",cur.PC ,old.PC );
				DrwReg("EFL",cur.FLAGS ,old.FLAGS );
				INC(y,7);
				DrwReg("CS",cur.CS ,old.CS );
				DrwReg("DS",cur.DS ,old.DS );
				DrwReg("SS",cur.SS ,old.SS );
				DrwReg("ES",cur.ES ,old.ES );
				y:=15;l:=105;
				FOR i:=0 TO FlCount-1 DO
				 DrwFlag(i);
				END;
		END DrawBackground;
		
	END RegPanel;
	
	CONST
	
		clrSelection* = SIGNED32(08080FFFFH);
		clrCursor* = SIGNED32(0FFFFFFFFH);
		clrBackGround* = SIGNED32(000080FFH);
		clrText* = SIGNED32(0FFFFFFH);
	TYPE
		EditState = ENUM edAddr, edHex, edText END;
		
	Highlight* = POINTER TO RECORD
		enable:BOOLEAN;
		kind*:SIGNED32;
		from*, to*:SIGNED32;
		color*:SIGNED32;
		mem*:POINTER TO ARRAY OF CHAR;
	END;
	
		
	MemPanel* = OBJECT(WMComponents.VisualComponent)
	VAR
		font:WMGraphics.Font;
	
		(* editState : EditState; *)

(*TODO: Make property*)
		selectionColor-,
		backGroundColor-,
		textColor- : SIGNED32;
		fontHeight:SIGNED32; fontWidth:SIGNED32;

		addrVisible*:BOOLEAN;
		addrWidth:SIGNED32;

		textTop,
		textLeft:SIGNED32;
		
		cursorIsVisible:BOOLEAN;
		cursor:SIGNED32;
		selection:Highlight;
		
		address-:WMProperties.Int32Property;
		adr:ADDRESS;

		PROCEDURE SetFont(f:WMGraphics.Font);
		VAR
			g : WMGraphics.GlyphSpacings;
		BEGIN
			font:=f;
			fontHeight:=font.GetHeight();
			textTop:= fontHeight;
			font.GetGlyphSpacings(ORD('0'), g);
			fontWidth:=g.bearing.l + g.width + g.bearing.r;
			addrVisible:=TRUE;
			addrWidth:=fontWidth*9;
			textLeft:=addrWidth+(16*3+6)*fontWidth;
			Invalidate();
		END SetFont;
		
		PROCEDURE &Init*();
		VAR
			(* i:SIGNED32; *)
		BEGIN
			Init^;
			SetNameAsString(Strings.NewString("MemPanel"));
			NEW(address, NIL,  Strings.NewString("Address"), Strings.NewString("Set begin address")); properties.Add(address);
			NEW(selection);
			selection.enable:=FALSE;
			selection.from:=-1;
			selection.to:=-1;
			
			adr:=SELF;

			font:=WMGraphics.GetFont("Courier", 12, {});
			fillColor.Set (clrBackGround);
(*			textColor:=SIGNED32(0FFFF00FFH);*)

			textColor:=clrText;
			selectionColor:=clrSelection;
			
			SetNameAsString(Strings.NewString("MemPanel"));
			SetFont(WMGraphics.GetFont((*"Courier"*)"Courier", 12, {0}));

			cursor:=0;
(*			KernelLog.String("fontHeight= ");KernelLog.Int(fontHeight,0); KernelLog.Ln; 
			KernelLog.String("fontWidth= ");KernelLog.Int(fontWidth,0); KernelLog.Ln; *)
			SetPointerInfo(manager.pointerText);

			takesFocus.Set(TRUE);

(*			WMStandardComponents.blinker.events.Add(SetBlinker);*)
		END Init;

		PROCEDURE PropertyChanged*(sender, property : ANY);
		BEGIN
			IF (property = address) THEN
				adr := address.Get();
				Invalidate;
			ELSE
				PropertyChanged^(sender, property)
			END
		END PropertyChanged;


		PROCEDURE GetCursorRect():WMRectangles.Rectangle;
		VAR
			(* cliprect : WMRectangles.Rectangle; *)
			pos:SIGNED32;
			x,y:SIGNED32;
		BEGIN
			pos:=cursor DIV 2;
			x:=pos MOD 16;
			x:=fontWidth*(2+x*3+(cursor MOD 2)+(x DIV 4)+(x DIV 8) )+addrWidth;
			y:=fontHeight*(1+pos DIV 16)+textTop;
			RETURN WMRectangles.MakeRect(x,y,x+fontWidth,y+2);
		END GetCursorRect;

		PROCEDURE GetLineCount():SIGNED32;
		BEGIN
			RETURN (bounds.GetHeight() DIV fontHeight);
		END GetLineCount;

		PROCEDURE FindLineByY(y:SIGNED32):SIGNED32;
		BEGIN
			RETURN ((y-textTop) DIV fontHeight);
		END FindLineByY;
		PROCEDURE CheckPointer(a,b:ADDRESS):BOOLEAN;
		BEGIN
			IF a<04000000H THEN
			IF (a+b) DIV 10000H>=((1+a DIV 10000H)) THEN RETURN FALSE ELSE RETURN TRUE END;
			END;
			RETURN TRUE;
		END CheckPointer;

		
		PROCEDURE Draw*(canvas : Canvas);
		VAR
			i, j, pos, y, x, xi : SIGNED32;
			tmps : ARRAY 9 OF CHAR; 
			(* code : SIGNED32; 
			g : WMGraphics.GlyphSpacings; *)
			tmp : SIGNED32;
			cursr, cliprect : WMRectangles.Rectangle; cstate : WMGraphics.CanvasState;
			ch : CHAR;
			la,lb : SIGNED32;
		BEGIN
			Draw^(canvas);
			canvas.SaveState(cstate);
			canvas.GetClipRect(cliprect);
			IF WMRectangles.RectEmpty(cliprect) THEN RETURN END;
	
			canvas.SetClipRect(cliprect);
			canvas.Fill(bounds.Get(), clrBackGround, WMGraphics.ModeSrcOverDst) ;
			canvas.SetColor(textColor);
			canvas.SetFont(font);
			
			la := FindLineByY(cliprect.t);
			lb := FindLineByY(cliprect.b);
			
			pos:=0;
			y:=0;xi:=0;
			x:=addrWidth; y:=fontHeight;
			tmp:=0;

			IF la<0 THEN (*if need render top area*)
				x:=addrWidth + fontWidth DIV 2;
				FOR j:=0 TO 0FH DO (*Render line*)
					IF (j MOD 4)=0 THEN INC(x,fontWidth); END;
					IF (j MOD 8)=0 THEN INC(x,fontWidth); END;
					Strings.IntToHexStr(j, 1, tmps);
					canvas.DrawString(x, textTop, tmps);				(*Render char code*)
					INC(x,fontWidth*3);
				END;
				la:=0;
			END;

			FOR i:=la TO lb DO
				y:=(i+1)*(fontHeight)+textTop; pos:=i*16; 
				(*Render line*)
				Strings.IntToHexStr(adr+pos, 8, tmps);
				Strings.Append(tmps, ":");
				canvas.DrawString(fontWidth,y,tmps);	(*Render address*)

				x:=addrWidth;
				FOR j:=0 TO 0FH DO (*Render line*)
					IF (j MOD 4)=0 THEN INC(x,fontWidth); END;
					IF (j MOD 8)=0 THEN INC(x,fontWidth); END;
					IF (adr+pos)>1024 THEN
						 	IF CheckPointer(adr,pos) THEN
								SYSTEM.GET(adr+pos,ch);				(*TODO: make protect to read N/A address*)
						 	ELSE
									ch:=0X;				(*TODO: make protect to read N/A address*)
						 	END;
					ELSE
						ch:=0X;
					END;
					Strings.IntToHexStr(ORD(ch), 2, tmps);
					canvas.DrawString(x,y,tmps);				(*Render char code*)

					(*Draw selection bkgrnd*)
					IF ((adr+pos)>=MIN(selection.from,selection.to))&((adr+pos)<MAX(selection.from,selection.to)) THEN
						canvas.Fill(
						WMGraphics.MakeRectangle(textLeft+(j)*fontWidth,y - fontHeight,textLeft+(j+1)*fontWidth, y),
						clrSelection, WMGraphics.ModeSrcOverDst) ;
					
					END;

					(*Draw selection cursor*)
					IF pos=(cursor DIV 2) THEN
						canvas.Fill(
						WMGraphics.MakeRectangle(textLeft+(j)*fontWidth,y - fontHeight,textLeft+(j+1)*fontWidth, y),
						clrCursor, WMGraphics.ModeSrcOverDst) ;
					END;

					
					IF font.HasChar(ORD(ch)) THEN
						font.RenderChar(canvas,textLeft+j*fontWidth,y,ORD(ch));(*Render char*)
					END;
					INC(pos);INC(x,fontWidth*3);
				END;
			END;
			
			cursr:=GetCursorRect();
			IF cursorIsVisible THEN
				canvas.Fill(cursr,SIGNED32(0C0C040FFH),WMGraphics.ModeSrcOverDst);
			ELSE
				canvas.Fill(cursr,fillColor.Get(),WMGraphics.ModeSrcOverDst);
			END;

			canvas.RestoreState(cstate);

	END Draw;
	
		PROCEDURE WheelMove*(dz: SIZE); (** PROTECTED *)
		BEGIN
			IF mouseWheelScrollSpeedI # 0 THEN
				address.Set(adr+16*mouseWheelScrollSpeedI*dz);
			END;
			Invalidate();

		END WheelMove;

		PROCEDURE ViewToTextPos(x, y : SIGNED32; VAR st:EditState):SIGNED32;
				PROCEDURE GetHexX(x:SIGNED32):SIGNED32;
				VAR
				 (* pos,i:SIGNED32;
				 g1,g2:SIGNED32;
				 sx:SIGNED32; *)
				BEGIN
					x:=x DIV fontWidth;
					x:=x-((x+1) DIV 27);					
					x:=x-((x+1)  DIV 13);
					x:=x-((x+1) DIV 3);
					RETURN x;
				END GetHexX;

		BEGIN
			y := (y - textTop) DIV fontHeight;
			y := MAX(y, 0);
			IF x<addrWidth+fontWidth*2 THEN
				st:=EditState.edAddr;
			ELSIF x<textLeft THEN
				st:=EditState.edHex;
				x:=GetHexX(x-addrWidth-fontWidth*2);
				cursor:=y*32+x;
			ELSE
				st:=EditState.edText;
				x:=MIN((x-textLeft) DIV fontWidth, 15);
			END;

(*			KernelLog.String("dh= "); KernelLog.Hex(SIGNED32(st),8); 
			KernelLog.String("x= "); KernelLog.Int(x, 0); 
			KernelLog.String(" y= "); KernelLog.Int(y, 0); KernelLog.Ln;*)
			RETURN 0;
		END ViewToTextPos;
		
		
		PROCEDURE PointerDown*(x, y : SIZE; keys : SET);
		VAR pos : SIGNED32;
				st:EditState
		BEGIN
			pos:=ViewToTextPos(x,y,st);
			cursorIsVisible := TRUE;
			Invalidate;
		END PointerDown;

		(* MemPanel *)
		PROCEDURE Handle(VAR m: WMMessages.Message);
		BEGIN
			IF m.msgType = WMMessages.MsgKey THEN
				IF ~HandleShortcut(m.x, m.flags, m.y) THEN
					Handle^(m);
				END;
			ELSE Handle^(m)
			END
		END Handle;

		(** *)
		PROCEDURE SetSelection(select:BOOLEAN;old:SIGNED32);
		VAR
		BEGIN
			IF select THEN
				
				IF ~selection.enable THEN
					selection.from:=adr+(old DIV 2);
				END;
				selection.to:=adr+((cursor+1) DIV 2);
				
				selection.enable:=TRUE;
					
			ELSE
				selection.enable:=FALSE;
(*				selection.from:=-1;
				selection.to:=-1;*)
			END
		END SetSelection;
		
		PROCEDURE HandleShortcut(ucs : SIGNED32; flags : SET; keysym : SIGNED32) : BOOLEAN;
		VAR
			tmp : ADDRESS;
			oldcur:SIGNED32;
		BEGIN
(*			KernelLog.String("dh= "); KernelLog.Hex(keysym, 8);KernelLog.Hex(SYSTEM.VAL(SIGNED32,flags), 8);  KernelLog.Ln;*)
				oldcur:=cursor;
				IF (keysym= 0FF51H)  THEN (* LEFT *)
					IF	cursor>0 THEN
						DEC(cursor);
					ELSE
						cursorIsVisible := TRUE;
						address.Set(adr-1);
						RETURN FALSE; (*small optimize, no need do invalidate twice*)
					END;
				ELSIF (keysym= 0FF52H) THEN (* UP *)
					IF cursor<16*2 THEN
						cursorIsVisible := TRUE;
						address.Set(adr-16);
						RETURN FALSE; (*small optimize, no need do invalidate twice*)
					ELSE
						DEC(cursor,16*2);cursor:=MAX(0,cursor);
					END;
				ELSIF (keysym= 0FF53H)  THEN (* RIGHT *)
					IF cursor<(GetLineCount()*32-1) THEN
						INC(cursor);cursor:=cursor;
					ELSE
						cursorIsVisible := TRUE;
						address.Set(adr+1);
						RETURN FALSE; (*small optimize, no need do invalidate twice*)
					END;
				ELSIF (keysym= 0FF54H)  THEN (* DOWN *)
					IF cursor<(GetLineCount()-1)*32 THEN
						INC(cursor,16*2);
					ELSE
						cursorIsVisible := TRUE;
						address.Set(adr+16);
						RETURN FALSE; (*small optimize, no need do invalidate twice*)
					END;
				ELSIF keysym = 0FF56H THEN (* Page Down *)
					cursorIsVisible := TRUE;
					address.Set(adr+(GetLineCount()-1)*16);
					RETURN FALSE; 
				ELSIF keysym = 0FF55H THEN (* Page Up *)
					cursorIsVisible := TRUE;
					address.Set(adr-(GetLineCount()-1)*16);
					RETURN FALSE; 
				ELSIF keysym = 0FF50H THEN (* Cursor Home *)
					cursor:=(cursor DIV 32)*32
				ELSIF keysym = 0FF57H THEN (* Cursor End *)
					cursor:=(cursor DIV 32)*32+30
				ELSIF (keysym= 0FFBFH) & (flags = {})THEN (* F2 *)
					KernelLog.Memory(adr,GetLineCount()*32);
				ELSIF (keysym= 0FFC2H)& (flags = {}) THEN (* F5 *)
					SYSTEM.GET(adr,tmp);
					cursor:=0;
					cursorIsVisible := TRUE;
					address.Set(tmp);
					RETURN FALSE;
				END;
				SetSelection(flags * Inputs.Shift # {}, oldcur);(**********************)

				cursorIsVisible := TRUE;
				Invalidate();
			RETURN FALSE;
		END HandleShortcut;
		
		PROCEDURE SetBlinker(sender, data: ANY);
		BEGIN
			WITH sender: WMStandardComponents.Blinker DO
				cursorIsVisible := sender.visible;
			END;
			InvalidateRect(GetCursorRect());
		END SetBlinker;
		
		PROCEDURE FocusReceived;
		BEGIN
			FocusReceived^;
			cursorIsVisible := TRUE;
			WMStandardComponents.blinker.events.Add(SetBlinker);
			Invalidate;
		END FocusReceived;

		PROCEDURE FocusLost;
		BEGIN
			FocusLost^;
			WMStandardComponents.blinker.events.Remove(SetBlinker);
			cursorIsVisible := FALSE;
			Invalidate;
		END FocusLost;

		PROCEDURE Finalize;
		BEGIN
			Finalize^;
			WMStandardComponents.blinker.events.Remove(SetBlinker);
		END Finalize;

	END MemPanel;


	CodePanel* = OBJECT(WMComponents.VisualComponent)
	VAR 
		codelist : WMStringGrids.StringGrid;
		spacings : WMGrids.Spacings;
		
		PROCEDURE &Init*;
		BEGIN
			Init^;
			SetNameAsString(Strings.NewString("CodePanel"));

			NEW(codelist); 
			codelist.alignment.Set(WMComponents.AlignClient); 
			NEW(spacings, 4); 
			(*Address*)		(*Instr*)			(*Arg*)			(*Variables*)
			spacings[0] := 80; spacings[1] := 60;spacings[2] := 140;spacings[3] := 160;
			AddContent(codelist);
			codelist.Acquire;
			codelist.defaultRowHeight.Set(14);
			codelist.cellDist.Set(0);
			codelist.clCell.Set(SIGNED32(0FFFFFFA0H));
			codelist.SetColSpacings(spacings);
			codelist.SetFont(WMGraphics.GetFont("Courier", 10, {}));
			codelist.SetSelectionMode(WMGrids.GridSelectSingleRow);
			codelist.model.Acquire;
			codelist.model.SetNofCols(4);
			codelist.model.SetNofRows(1);
			codelist.model.SetCellText(0, 0, Strings.NewString("Adr"));
			codelist.model.SetCellText(1, 0, Strings.NewString("Instr"));
			codelist.model.SetCellText(2, 0, Strings.NewString("Arg"));
			codelist.model.SetCellText(3, 0, Strings.NewString("vars"));
			codelist.SetTopPosition(0, 0, TRUE);
			codelist.model.Release;
			codelist.Release;

		END Init;
		
		PROCEDURE StartNewOpcodes*(opcodes : BtDecoder.Opcode;ofs,pc:ADDRESS);
		VAR
			n, i, k : SIGNED32;
			p : BtDecoder.Opcode;
			st : Streams.StringWriter;
			s : Strings.String;
			value : ARRAY 255 OF CHAR;
		BEGIN
			n:=0;
			p:=opcodes;
			WHILE p # NIL DO
				INC(n);
				p := p.next;
			END;
			codelist.model.Acquire;
			codelist.model.SetNofRows(n+1);
			k:=0;
			FOR i:=1 TO n DO
				NEW(s,10);
				IF (opcodes.offset+ofs)=pc THEN 
					k:=i;
					codelist.model.SetCellColors(0, i, SIGNED32(0BCE0E0FFH), WMGraphics.Black);
					codelist.model.SetCellColors(1, i, SIGNED32(0BCE0E0FFH), WMGraphics.Black);
					codelist.model.SetCellColors(2, i, SIGNED32(0BCE0E0FFH), WMGraphics.Black);
					codelist.model.SetCellColors(3, i, SIGNED32(0BCE0E0FFH), WMGraphics.Black);
				ELSE
					codelist.model.SetCellColors(0, i, WMGraphics.White, WMGraphics.Black);
					codelist.model.SetCellColors(1, i, WMGraphics.White, WMGraphics.Black);
					codelist.model.SetCellColors(2, i, WMGraphics.White, WMGraphics.Black);
					codelist.model.SetCellColors(3, i, WMGraphics.White, WMGraphics.Black);
				END;
				Strings.IntToHexStr(opcodes.offset+ofs,8,s^);
				codelist.model.SetCellText(0, i, s);
				NEW(st, 20); opcodes.PrintInstruction(st);st.Get(value);
				codelist.model.SetCellText(1, i, Strings.NewString(value));

				NEW(st, 20); opcodes.PrintArguments(st);st.Get(value);
				codelist.model.SetCellText(2, i, Strings.NewString(value));

				NEW(st, 30); opcodes.PrintVariables(st);st.Get(value);
				codelist.model.SetCellText(3, i, Strings.NewString(value));
				opcodes := opcodes.next;
			END;
			codelist.SetTopPosition(0,MAX(0,k-3),TRUE);
(*			codelist.SetSelection(0,i,3,i);*)

			codelist.model.Release;

		END StartNewOpcodes;
	END CodePanel;

	
	Window* = OBJECT (WMComponents.FormWindow)
	VAR
		memPanel : MemPanel;
		(* varPanel : VariablesPanel; *)
		PROCEDURE CreateForm() : WMComponents.VisualComponent;
		VAR
			panel : WMStandardComponents.Panel;
			
		BEGIN
			NEW(panel);
			panel.bounds.SetExtents(800, 700);
			panel.fillColor.Set(SIGNED32(0FFFFFFFFH));
			panel.fillColor.Set(0BCE0E0FFH(*SIGNED32(0FFFFFFFFH)*));

			NEW(memPanel);
			memPanel.bounds.SetWidth (640);
			memPanel.bounds.SetHeight (600);
			memPanel.alignment.Set(WMComponents.AlignTop); 

			panel.AddContent(memPanel);
(*			NEW(varPanel);
			varPanel.bounds.SetWidth (300);
			varPanel.alignment.Set(WMComponents.AlignRight); 
			panel.AddContent(varPanel);
*)
			RETURN panel
		END CreateForm;

		PROCEDURE &New*;
		VAR vc : WMComponents.VisualComponent;
		BEGIN
			vc := CreateForm ();
			Init (vc.bounds.GetWidth (), vc.bounds.GetHeight (), FALSE);
			SetContent (vc);
			WM.DefaultAddWindow (SELF);
			SetTitle (Strings.NewString ("GUIPatW1"));
		END New;


	END Window;

VAR winstance : Window;
	manager : WM.WindowManager;

PROCEDURE Open*;
BEGIN
	IF winstance = NIL THEN NEW (winstance); END;	(* Only one window may be instantiated. *)
END Open;

PROCEDURE Test*;
BEGIN
(* *)
END Test;

PROCEDURE Cleanup;
BEGIN
	IF 	winstance # NIL THEN
		winstance.Close ();
		winstance := NIL
	END;
END Cleanup;
(** *)

BEGIN
	manager := WM.GetDefaultManager();

	flOfsArr := SYSTEM.VAL(FlOfsArr,[0, 2, 4, 6, 7,8,9 ,10,11,16,17,18,19,20,21]);
	flIdArr := SYSTEM.VAL(FlStrArr,[Strings.NewString("CF"),Strings.NewString("PF"),Strings.NewString("AF"), Strings.NewString("ZF"), 
	Strings.NewString("SF"), Strings.NewString("TF"), Strings.NewString("IF"), Strings.NewString("DF"), 
	Strings.NewString("OF"), Strings.NewString("RF"), Strings.NewString("VM"), Strings.NewString("AC"), 
	Strings.NewString("VIF"),Strings.NewString("VIP"),Strings.NewString("ID")]);
	 
	 flDescArr := SYSTEM.VAL(FlStrArr,[Strings.NewString("Carry Flag"),
	Strings.NewString("Parity Flag"),              Strings.NewString("Auxiliary Carry Flag"),     
	Strings.NewString("Zero Flag"),                Strings.NewString("Sign Flag"),                
	Strings.NewString("Trap Flag"),                Strings.NewString("Interrupt Enable Flag"),    
	Strings.NewString("Direction Flag"),          Strings.NewString("Overflow Flag"),            
	Strings.NewString("Resume Flag"),            Strings.NewString("Virtual-8086 Mode"),        
	Strings.NewString("Alignment Check"),      Strings.NewString("Virtual Interrupt Flag"),   
	Strings.NewString("Virtual Interrupt Pending"), Strings.NewString("ID Flag")]); 

	Modules.InstallTermHandler(Cleanup)
END BtDbgPanel.Open~
System.Free  BtDbgPanel WMDebugger~

WMDebugger.Open  WMDebugger/BtDbgPanel.Mod~
BtDbgPanel.Chek~
BtDbgPanel.Open~

0 	CF 	Carry Flag
2 	PF 	Parity Flag
4 	AF 	Auxiliary Carry Flag
6 	ZF 	Zero Flag
7 	SF 	Sign Flag
8 	TF 	Trap Flag
9 	IF 	Interrupt Enable Flag
10 	DF 	Direction Flag
11 	OF 	Overflow Flag
16 	RF 	Resume Flag
17 	VM 	Virtual-8086 Mode
18 	AC 	Alignment Check
19 	VIF 	Virtual Interrupt Flag
20 	VIP 	Virtual Interrupt Pending
21 	ID 	ID Flag

BtDbgPanel.Open~


