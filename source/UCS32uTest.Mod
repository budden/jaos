MODULE UCS32uTest; 

IMPORT UCS32, UCS32u, Commands, UTF8Strings, KernelLog;

(* Взято из UTF8Strings, перенести навсегда в UCS32. Хотел сделать перечислением, 
  но для некоторых типов ошибки при сравнении быть не может, поэтому нужен ЮНИОН из перечислений,
  а такого нет. Поэтому пусть остаётся набором констант *)
CONST
	CmpLess* = -1; CmpEqual* = 0; CmpGreater* = 1;	 CmpError* = 2; (** results for Compare. *)


PROCEDURE Delajj*(c : Commands.Context); 
VAR s1, s2 : ARRAY 100 OF CHAR;

PROCEDURE Tju(s1, s2 : ARRAY OF CHAR;ozhid : SIGNED8);
VAR u1, u2 : ARRAY 50 OF UCS32.Char;
    jq1, jq2 : ARRAY 50 OF UCS32.CharJQ;
    i : SIZE; poluchili : SIGNED8;
BEGIN 
i:=0; UTF8Strings.UTF8toUnicode(s1,u1,i);
i:=0; UTF8Strings.UTF8toUnicode(s2,u2,i);
UCS32.UCS32StringVUCS32StringJQ(u1,jq1);
UCS32.UCS32StringVUCS32StringJQ(u2,jq2);
poluchili := SIGNED8(UCS32.SravniStringJQ(jq1,jq2));
ASSERT(poluchili=ozhid);
END Tju;

BEGIN
KernelLog.String("UCS32uTest.Delajj..."); KernelLog.Ln;
COPY("ай",s1); COPY("айя",s2); 
Tju("ай","айя",CmpLess);
Tju("ай","ай",CmpEqual);
Tju("d","cc",CmpGreater);
Tju("","",CmpEqual);
KernelLog.String("UCS32uTest.Delajj: тесты прошли"); KernelLog.Ln;
END Delajj;

END UCS32uTest.Delajj