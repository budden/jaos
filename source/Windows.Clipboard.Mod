MODULE Clipboard; (** AUTHOR "ALEX"; PURPOSE "Windows clipboard interface"; *)

IMPORT Kernel32, User32, KernelLog, Modules, Texts, 
	HostClipboard, UCS32, UCS2, HostStrings;

(** Copy text of Windows clipboard to text *)
PROCEDURE GetFromClipboard( text: Texts.Text );
VAR
	clipWinString: HostStrings.StrokaWindowsUTF16DoNolja;
	clipA2UCS2String: UCS2.PString;
	textBuffer: UCS32.PString;
BEGIN
	ASSERT(( text # NIL ) & ( text.HasWriteLock( )));
	IF User32.OpenClipboard(Kernel32.NULL) # Kernel32.False THEN
		clipWinString := User32.GetClipboardData( User32.CFUnicodeText );
		IF clipWinString # Kernel32.NULL THEN
		(* copy clipboard data into internal buffer and unlock clipboard data *)
			clipWinString := Kernel32.GlobalLock( clipWinString );
			IF clipWinString = Kernel32.NULL THEN
				KernelLog.String("Otkaz pri blokirovke bufera obmena");
				KernelLog.Ln;
				RETURN END;
			clipA2UCS2String := HostStrings.ZagruziStrokuIzWindows( clipWinString );
			IF Kernel32.GlobalUnlock( clipWinString ) = Kernel32.False THEN
				KernelLog.String("Otkaz v razblokirovanii clipWinString"); 
				KernelLog.Ln END;
			IF User32.CloseClipboard( ) = Kernel32.False THEN
				KernelLog.String("Otkaz pri zakrytii bufera obmena"); 
				KernelLog.Ln END;
				
			textBuffer := UCS32.JQvVTotZhePString(UCS2.U2vNovJQ(clipA2UCS2String^));
			IF textBuffer # NIL THEN
				text.Delete( 0, text.GetLength( ));
				text.InsertUCS32( text.GetLength( ), textBuffer^ ) END END END END GetFromClipboard;


(** Copy text to Windows clipboard *)
PROCEDURE PutToClipboard( text: Texts.Text );
VAR
	clipboardData: HostStrings.StrokaWindowsUTF16DoNolja;
	textBuffer: UCS2.PString;
	textLen: SIZE;
BEGIN
	ASSERT(( text # NIL ) & ( text.HasReadLock( )));
	textLen := text.GetLength( );
	IF textLen > 0 THEN
		textBuffer := Texts.IzTextsTextVUTF16(text);
		textLen := LEN(textBuffer) + 1;

		IF User32.OpenClipboard( Kernel32.NULL ) # Kernel32.False THEN
			IGNORE User32.EmptyClipboard( );
			IGNORE HostStrings.OtpravqStrokuVWindowsDljaPutClipboard(textBuffer, clipboardData);
			IGNORE User32.SetClipboardData(User32.CFUnicodeText, clipboardData );
		END;
		IGNORE User32.CloseClipboard( );
	END;
END PutToClipboard;

PROCEDURE ClipboardChanged(sender, data : ANY);
BEGIN
	Texts.clipboard.AcquireRead;
	PutToClipboard(Texts.clipboard);
	Texts.clipboard.ReleaseRead;
END ClipboardChanged;

PROCEDURE Install*;
BEGIN
	KernelLog.Enter; KernelLog.String("WindowsClipboard: Registered clipboard at host clipboard interface."); KernelLog.Exit;
END Install;

PROCEDURE Cleanup;
BEGIN
	Texts.clipboard.onTextChanged.Remove(ClipboardChanged);
	HostClipboard.SetHandlers(NIL, NIL);
	KernelLog.Enter; KernelLog.String("WindowsClipboard: Unregistered clipboard at host clipboard interface."); KernelLog.Exit;
END Cleanup;


BEGIN
	(* register with AosText clipboard *)
	Texts.clipboard.onTextChanged.Add( ClipboardChanged );
	HostClipboard.SetHandlers( GetFromClipboard, PutToClipboard );
	Modules.InstallTermHandler( Cleanup );
END Clipboard.

Clipboard.Install ~

System.Free Clipboard ~