(* r -- komandy dlja refleksii i otladochnojj pechati iz modulja *.PodrobnajaPechatq.Mod *)
MODULE r; IMPORT Commands, Modules, PodrobnajaPechatq, Streams, Reflection
	,Strings, SYSTEM, KernelLog, Trace, TrapWriters;

PROCEDURE ZagruziModulqIliObjqjasnisq(imjaModulja : ARRAY OF CHAR; ktoVyzval : ARRAY 256 OF CHAR;
		VAR o : Streams.Writer): Modules.Module;
	VAR kodOshibki : SIGNED32; mod : Modules.Module; msg : ARRAY 4096 OF CHAR;
	BEGIN
		mod := Modules.ModuleByName(imjaModulja);
		IF mod # NIL THEN RETURN mod END;
		(* Zagruzka modulja vypolnjaet kod, nado o nejj soobshitq *)
		o.String("(");
		o.String(ktoVyzval);
		o.String(": zagruzhaem modulq "); o.String(imjaModulja); o.String(")");
		mod := Modules.ThisModule(imjaModulja, kodOshibki, msg);
		IF (mod = NIL) OR (kodOshibki # 0) THEN
			mod := NIL;
			o.String("("); o.String(ktoVyzval); o.String(": ne smogli poluchitq modulq: ");
			o.Int(kodOshibki, 0); o.String(";"); o.String(msg); o.String(")") 
		ELSE
			o.String(" Zagruzka modulja ok ") END;
		o.Ln;
		RETURN mod;
	END ZagruziModulqIliObjqjasnisq;

PROCEDURE RazberiAdres(o : Streams.Writer; CONST adresSPrefiksom : ARRAY OF CHAR; VAR adres : ADDRESS) : BOOLEAN;
	VAR uspekh : SIGNED32; adresL : SIGNED32; hexS : Strings.String;
BEGIN
	IF (adresSPrefiksom[0] = "&") OR (adresSPrefiksom[0] = "^") THEN
		hexS := Strings.Substring2(1, adresSPrefiksom)
	ELSE
		hexS := Strings.NewString(adresSPrefiksom) END;
	Strings.HexStrToInt(hexS^, adresL, uspekh);
	IF uspekh # Strings.Ok THEN
		o.String("(Nevernyjj 16-richnyjj adres objekta)"); o.Ln;
		RETURN FALSE;
	ELSE
		adres := SYSTEM.VAL(ADDRESS, adresL);
		RETURN TRUE; END END RazberiAdres;

(* STz pechataet s pomoshhju WriteValue *)
PROCEDURE STzВнутр*(context : Commands.Context; отклПроверкуАдреса: BOOLEAN);
VAR mod: Modules.Module;
VAR adresSPrefiksom, dannyeOTipe, imjaModulja : ARRAY 256 OF CHAR;
	 adres : ADDRESS; offset : SIZE; o : Streams.Writer;

	PROCEDURE PechSpravku;
	BEGIN
		o.String("(Format vyzova: `r.STz: adres dannye-o-tipe [`imja tipa']~'");  o.Ln;
		o.String(" adres mozhet bytq v vide 7777FFF0, &7777FFF0, ^7777FFF0"); o.Ln;
		o.String(" dannye o tipe - v vide Modulq:F03, gde F03 - smeshhenie vnutri refs. "); o.Ln;
		o.String(" `Imja tipa' ignoriruetsja)"); o.Ln;
	END PechSpravku;


	PROCEDURE RazberiDannyeOTipe(): BOOLEAN;
	VAR razbito : Strings.StringArray; offsetS : ARRAY 256 OF CHAR;
		val, uspekh : SIGNED32;
	BEGIN
		razbito := Strings.Split(dannyeOTipe, ':');
		IF LEN(razbito) # 2 THEN
			o.String("(r.STz: V dannykh o tipe dolzhna bytq rovno odna`:')"); o.Ln;
			RETURN FALSE END;
		COPY(razbito[0]^,imjaModulja); COPY(razbito[1]^, offsetS);
		Strings.HexStrToInt(offsetS, val, uspekh);
		IF uspekh # Strings.Ok THEN
			o.String("(r.STz: Nevernoe 16-richoe smeshhenie)"); o.Ln;
			RETURN FALSE END;
		offset := val; (* Hm, kak naschjot perepolnenija? *)
		mod := ZagruziModulqIliObjqjasnisq(imjaModulja, "r.STz", o);
		RETURN (mod # NIL)
	END RazberiDannyeOTipe;

	PROCEDURE NapechatajjEhto;
	VAR оит: PodrobnajaPechatq.ОбъектИТип; 
	VAR k : PodrobnajaPechatq.KontekstPechati; мусор: SIZE;
	BEGIN
		PodrobnajaPechatq.ZadajjKontekstPechatiPoUmolch(k);
		оит.адрВнутр := adres;
		оит.refsOffset := offset;
		оит.mod := mod;
		k.w := o;
		o.Ln; o.String("========="); o.Ln;
		PodrobnajaPechatq.WriteAValue(k, оит, мусор, TRUE, TRUE, отклПроверкуАдреса) END NapechatajjEhto;


BEGIN {EXCLUSIVE} (* Ne znaju, zachem EXCLUSIVE *)
	o := context.out;
	IF ~context.arg.GetString(adresSPrefiksom) OR ~context.arg.GetString(dannyeOTipe) THEN
		PechSpravku; RETURN END;

	IF ~RazberiAdres(o, adresSPrefiksom, adres) THEN RETURN END;
	IF ~RazberiDannyeOTipe() THEN RETURN END;
	NapechatajjEhto;
	END STzВнутр;
	
	
PROCEDURE STd*(context: Commands.Context);
BEGIN STzВнутр(context, TRUE) END STd;

PROCEDURE STz(context: Commands.Context);
BEGIN STzВнутр(context, FALSE) END STz;
	
(* Pech chitaet adres dannykh i pechataet dinamicheski tipizorovannoe znachenie po ehtomu adresu.
 Ehta procedura opasna - ispolqzovatq tolko dlja otladki. *)
PROCEDURE Pech*(context : Commands.Context); 
VAR adresSPrefiksom : ARRAY 256 OF CHAR; adres : ADDRESS; o : Streams.Writer; a : ANY;
BEGIN 
	o := context.out;
	IF ~context.arg.GetString(adresSPrefiksom) THEN
		o.String("(Format vyzova: `r.Pech adres'"); o.Ln;
		o.String(" pechataet dynamich-tipizirovannye dannye po ehtomu adresu)"); o.Ln;
		RETURN END;
	IF ~RazberiAdres(o, adresSPrefiksom, adres) THEN RETURN END;
	a := SYSTEM.VAL(ANY, adres);
	o.Ln; o.String("========="); o.Ln;
	PodrobnajaPechatq.Pech(context.out, a) END Pech;


PROCEDURE PokazhiSostojanieModulja*(context : Commands.Context);
VAR m: Modules.Module;
VAR imjaModulja : ARRAY 256 OF CHAR; o : Streams.Writer;
BEGIN {EXCLUSIVE} (* Ne znaju, zachem EXCLUSIVE *)
	IF ~context.arg.GetString(imjaModulja) THEN
		context.out.String("PokazhiSostojanieModulja: Ozhidalosq imja modulja"); RETURN END;
	o := context.out;
	m := ZagruziModulqIliObjqjasnisq(imjaModulja, "r.PokazhiSostojanieModulja", o);
	IF (m = NIL) THEN RETURN END;
	o.String("========= PokazhiSostojanieModulja: ");
	o.String(imjaModulja);
	o.String(" ==========");
	o.Ln;
	PodrobnajaPechatq.ModuleState(o, m);
END PokazhiSostojanieModulja;


PROCEDURE PokazhiSostojanieModuljaP*(CONST imjaModulja : ARRAY OF CHAR; o : Streams.Writer);
VAR m: Modules.Module;
BEGIN {EXCLUSIVE} (* Ne znaju, zachem EXCLUSIVE *)
	m := ZagruziModulqIliObjqjasnisq(imjaModulja, "r.PokazhiSostojanieModulja", o);
	IF (m = NIL) THEN RETURN END;
	o.String("========= PokazhiSostojanieModulja: ");
	o.String(imjaModulja);
	o.String(" ==========");
	o.Ln;
	PodrobnajaPechatq.ModuleState(o, m)
END PokazhiSostojanieModuljaP;


PROCEDURE ReportModuleByName*(context : Commands.Context);
VAR m: Modules.Module;
VAR res : SIGNED32; imjaModulja, msg : ARRAY 4096 OF CHAR; o : Streams.Writer;
BEGIN {EXCLUSIVE} (* Ne znaju, zachem EXCLUSIVE *)
	IF ~context.arg.GetString(imjaModulja) THEN
		context.out.String("ReportModuleByName: Ozhidalosq imja modulja"); RETURN END;
	o := context.out;
	o.String("Zagruzhaju modulq"); o.Ln;
	m := Modules.ThisModule(imjaModulja, res, msg);
	IF (m = NIL) OR (res # 0) THEN
		o.String("ReportModuleByName: ne smog zagruzitq modulq:"); o.String(msg); o.Ln;
		RETURN END;
	o.String("========= PokazhiSostojanieModulja: ");
	o.String(imjaModulja);
	o.String(" ==========");
	o.Ln;
	Reflection.ReportModule(o, m.refs, 0);
	END ReportModuleByName;

(* TraceBackKL печатает диапазон кадров стека в KernelLog. Надо задавать from не менее 2 *)
PROCEDURE TraceBackKL*(from, to : SIZE);
VAR kl : Streams.Writer;
BEGIN
	Streams.OpenWriter(kl, KernelLog.Send);
	PodrobnajaPechatq.TraceBack(kl, from, to);
	kl.Update(); END TraceBackKL;


(* TraceBackTR печатает диапазон кадров стека в Trace. Ставь from>2, чтобы не печатать лишнего *)
PROCEDURE TraceBackTR*(from, to : SIZE);
VAR tr : Streams.Writer;
BEGIN
	Streams.OpenWriter(tr, Trace.Send);
	PodrobnajaPechatq.TraceBack(tr, from, to);
	tr.Update(); END TraceBackTR;


PROCEDURE PechKL*(x : ANY);
VAR kl : Streams.Writer;
BEGIN
	Streams.OpenWriter(kl, KernelLog.Send);
	PodrobnajaPechatq.Pech(kl, x);
	kl.Update(); END PechKL;


PROCEDURE PechTR*(x : ANY);
VAR tr : Streams.Writer;
BEGIN
	Streams.OpenWriter(tr, KernelLog.Send);
	PodrobnajaPechatq.Pech(tr, x);
	tr.Update(); END PechTR;


PROCEDURE Эхо*(context : Commands.Context);
VAR с : ARRAY 256 OF CHAR; o : Streams.Writer;
BEGIN 
	IF ~context.arg.GetString(с) THEN
		context.out.String("БуквальноЭто: ожидалась строка в формате команды ЯОС"); RETURN END;
	o := context.out;
	o.Ln;
	o.String(с);
	o.Ln;
END Эхо;
	

PROCEDURE Cleanup;
BEGIN
TrapWriters.StackTraceBackProcVar := 	Reflection.StackTraceBack; 
END Cleanup;

PROCEDURE PechatatqStekPodrobno*;
BEGIN
	Modules.InstallTermHandler(Cleanup);
	TrapWriters.StackTraceBackProcVar := PodrobnajaPechatq.StackTraceBack END PechatatqStekPodrobno;
END r.

