(* Modulq RashirenieOpcijjKompiljatora sluzhit dlja poluchenija realqnykh opcijj compiljatora 
   iz komandy zapuska kompiljator (po analogii s PET...FindPC, TFPET...DoCompile) *)
   
MODULE LisRasshirenieOpcijjKompiljatora;

IMPORT Diagnostics, LisCompiler;

TYPE
	ExtractCompilerOptionsDiagnostics* = OBJECT(Diagnostics.StreamDiagnostics);
		VAR
			compilerOptions* : LisCompiler.CompilerOptions; 
	END ExtractCompilerOptionsDiagnostics;

END LisRasshirenieOpcijjKompiljatora.
