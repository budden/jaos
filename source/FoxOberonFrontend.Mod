MODULE FoxOberonFrontend; (**  AUTHOR "fof"; PURPOSE "Oberon FoxCompiler: Oberon frontend module"; Копипаста с FoxOberonFrontend.Mod **)

IMPORT
	Streams, Diagnostics,  SyntaxTree := FoxSyntaxTree, Parser := FoxParser, Scanner := FoxScanner,FoxFrontend, Compiler, KernelLog;

TYPE

	Frontend* = OBJECT (FoxFrontend.Frontend)
	VAR
		scanner: Scanner.Scanner;
		parser: Parser.Parser;

		PROCEDURE Initialize*(diagnostics: Diagnostics.Diagnostics; flags: SET; reader: Streams.Reader; CONST fileName, definitions: ARRAY OF CHAR; pos: SIGNED32);
		BEGIN
			Initialize^(diagnostics, flags, reader, fileName, definitions, pos);
			scanner := Scanner.NewScanner(fileName, reader, pos, diagnostics);
			scanner.useLineNumbers := Compiler.UseLineNumbers IN flags;
			parser := Parser.NewParser( scanner, diagnostics, definitions );
		END Initialize;

		PROCEDURE Parse*(): SyntaxTree.Module;
		BEGIN
			RETURN parser.Module();
		END Parse;

		PROCEDURE Error*(): BOOLEAN;
		BEGIN
			RETURN parser.error;
		END Error;

		PROCEDURE Done*(): BOOLEAN;
		BEGIN
			RETURN ~parser.NextModule()
		END Done;

	END Frontend;

	PROCEDURE Get*():FoxFrontend.Frontend;
	VAR frontend: Frontend;
	BEGIN
		KernelLog.String("Зашёл в FoxOberonFrontend.Get"); KernelLog.Ln;
		NEW(frontend);
		RETURN frontend;
	END Get;

END FoxOberonFrontend.
