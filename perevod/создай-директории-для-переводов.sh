#!/bin/bash

set -xeuo pipefail 

SCRIPT_PATH=$(cd $(dirname $0) && pwd);
export JAOS_ROOT=`readlink -f $SCRIPT_PATH`/..

echo $JAOS_ROOT

mkdir -p $JAOS_ROOT/perevod/Win32Nano/ru
mkdir -p $JAOS_ROOT/perevod/Linux64Nano/ru

