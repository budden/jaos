MODULE TFPETUCS32;
	IMPORT KernelLog, Modules, Commands, Options, Streams, Inputs, Files, WMRestorable, XML, XMLObjects, WMStandardComponents, WMGraphics, CompilerInterface, WMComponents, WMRectangles, WMMessages, WMDialogs, WMTextView, WMEditors, ASCIIStrings := Strings, StringsUCS32, TextUtilities, Texts, WMWindowManager, WMGrids, WMMacros, WMPopups, WMDropTarget, WMDiagnostics, PETTrees, Configuration, Codecs, WMTabComponents, UndoManager, WMSearchComponents, WhitespaceRemover, TFModuleTrees := TFModuleTreesUCS32, TFScopeTools := TFScopeToolsUCS32, TS := TFTypeSysUCS32, Diagnostics, RasshirenieOpcijjKompiljatora, TFAOParser := TFAOParserUCS32, BimboScanner := BimboScannerUCS32, UCS32;
CONST 
	WindowWidth = 800; 
	WindowHeight = 600; 
	DoCompileModeCompile = 0; 
	DoCompileModeFindPC = 1; 
	DoCompileModeObtainConditionalDefinitions = 2; 
	No = 0; 
	Yes = 1; 
	Paranoid = 2; 
	DefaultBackupOnStore = No; 
	DefaultBackupOnCompile = FALSE; 
	DefaultShowPathInTabs = FALSE; 
	DefaultScratchPanelHeight = 250; 
	DefaultEnableWhitespaceWarnings = FALSE; 
	DefaultShowLineNumbers = FALSE; 
	DefaultIndicateTabs = FALSE; 
	DefaultCurrentLineColor = 0; 
	DefaultCompilerFindPC = TRUE; 
	DefaultSearchWrap = FALSE; 
	DefaultSearchCaseSensitive = TRUE; 
	DefaultSearchHighlightAll = FALSE; 
	DisableShortcuts = FALSE; 
	SearchStringMaxLen = 128; 
	MaxNbrOfTabs = 100; 
	MaxNbrOfCompilers = 8; 
	EditorFocus = 1; 
	SplitEditorFocus = 2; 
TYPE 
	CompilerOptions = ARRAY 256 OF UCS32.CharJQ; 

	Filename = ARRAY 256 OF UCS32.CharJQ; 

	ASCIIFilename = ARRAY (256 * 6) OF CHAR; 

	String = ARRAY 128 OF UCS32.CharJQ; 

	SearchString = ARRAY SearchStringMaxLen OF UCS32.CharJQ; 

	CompilerSettings = RECORD 
		name: ARRAY 32 OF UCS32.CharJQ; 
		caption: ARRAY 16 OF UCS32.CharJQ; 
		options: CompilerOptions; 
		fileExtension: ARRAY 16 OF UCS32.CharJQ; 
		loadmodule: Filename; 
		genTree: ARRAY 128 OF UCS32.CharJQ; 
		findPC: BOOLEAN; 
	END; 

	Settings = OBJECT 
	VAR 
		backupOnStore: SIGNED32; 
		backupOnCompile: BOOLEAN; 
		showPathInTabs: BOOLEAN; 
		scratchPanelHeight: SIGNED32; 
		enableWhitespaceWarnings: BOOLEAN; 
		showLineNumbers: BOOLEAN; 
		indicateTabs: BOOLEAN; 
		currentLineColor: SIGNED32; 
		defaultCompilerOptions: CompilerOptions; 
		defaultCompilerSettings: CompilerSettings; 
		compilers: ARRAY MaxNbrOfCompilers OF CompilerSettings; 
		nofCompilers: SIGNED32; 
		diffCommand, diffPrefix, diffSuffix: String; 
		searchWrap, searchHighlightAll, searchCaseSensitive: BOOLEAN; 

		PROCEDURE ^  & Init*; 
		PROCEDURE ^ GetCompilerSettings(CONST filename: UCS32.StringJQ): CompilerSettings; 
		PROCEDURE ^ LoadCompilerSettings; 
		PROCEDURE ^ Load; 
	END Settings; 

	CaptionObject = OBJECT 
	VAR 
		caption: ARRAY 128 OF UCS32.CharJQ; 

		PROCEDURE ^  & New*(CONST caption: ARRAY OF UCS32.CharJQ); 
	END CaptionObject; 

	Position = OBJECT 
	VAR 
		marker: WMTextView.PositionMarker; 
		ucs, keysym: SIZE; 
		flags: SET; 
		next: Position; 

		PROCEDURE ^  & Init*(ucs, keysym: SIZE; flags: SET); 
	END Position; 

	Positions = OBJECT 
	VAR 
		textView: WMTextView.TextView; 
		positions: Position; 

		PROCEDURE ^  & Init*(textView: WMTextView.TextView); 
		PROCEDURE ^ FindPosition(ucs, keysym: SIZE; flags: SET): Position; 
		PROCEDURE ^ StoreCurrentPosition(ucs, keysym: SIZE; flags: SET); 
		PROCEDURE ^ RecallPosition(ucs, keysym: SIZE; flags: SET); 
	END Positions; 

	ScratchPanel = OBJECT {EXCLUSIVE} (WMComponents.VisualComponent)
	VAR 
		editor: WMEditors.Editor; 
		label: WMStandardComponents.Label; 

		PROCEDURE ^  & Init*; 
		PROCEDURE ^ SetText(text: Texts.Text); 
		PROCEDURE ^ Finalize*; 
	END ScratchPanel; 

	URLDropTarget = OBJECT (WMDropTarget.DropTarget)
	VAR 
		win: Window; 

		PROCEDURE ^  & New*(win: Window); 
		PROCEDURE ^ GetInterface*(type: SIGNED32): WMDropTarget.DropInterface; 
	END URLDropTarget; 

	DropURL = OBJECT (WMDropTarget.DropURLs)
	VAR 
		win: Window; 

		PROCEDURE ^  & New*(win: Window); 
		PROCEDURE ^ URL*(CONST url: ARRAY OF CHAR; VAR res: INTEGER); 
	END DropURL; 

	PETPanel = OBJECT {EXCLUSIVE} (WMComponents.VisualComponent)
	VAR 
		editor, splitEditor: WMEditors.Editor; 
		logEdit: WMEditors.Editor; 
		scratchPanel, splitPanel: WMStandardComponents.Panel; 
		scratch: ScratchPanel; 
		sidePanel: WMStandardComponents.Panel; 
		logPanel, editPanel: WMStandardComponents.Panel; 
		searchPanel: WMSearchComponents.SearchPanel; 
		errorGrid: WMDiagnostics.DiagnosticsView; 
		diagnostics: WMDiagnostics.Model; 
		tree: PETTrees.Tree; 
		modified, splitted, wrap: BOOLEAN; 
		focus: SIGNED32; 
		codecFormat: ARRAY 128 OF UCS32.CharJQ; 
		autoCodecFormat: ARRAY 128 OF UCS32.CharJQ; 
		name: Filename; 
		filename: Filename; 
		options: CompilerOptions; 
		compilerSettings: CompilerSettings; 
		showErrorMarkers: BOOLEAN; 
		positions: Positions; 
		owner: Window; 

		PROCEDURE ^  & InitPanel*(window: Window); 
		PROCEDURE ^ CreateSidePanel(settings: CompilerSettings); 
		PROCEDURE ^ OnGoToFile(sender, data: ANY); 
		PROCEDURE ^ OnGoToDefinition(sender, data: ANY); 
		PROCEDURE ^ OnNodeExpand(sender, data: ANY); 
		PROCEDURE ^ HandleTreeRefresh(sender, data: ANY); 
		PROCEDURE ^ ClearLog; 
		PROCEDURE ^ RefreshWorkOfPETPanelInner(): TS.Module; 
		PROCEDURE ^ IsItActiveOberonSource(): BOOLEAN; 
		PROCEDURE ^ DoCompile(mode: SIGNED16; CONST pc: ARRAY OF UCS32.CharJQ; options: CompilerOptions): Options.Parameter; 
		PROCEDURE ^ ErrorClick(sender, data: ANY); 
		PROCEDURE ^ GoToNextError(forward: BOOLEAN); 
		PROCEDURE ^ EditorFocusHandler(hasFocus: BOOLEAN); 
		PROCEDURE ^ SplitEditorFocusHandler(hasFocus: BOOLEAN); 
		PROCEDURE ^ ToggleErrorsAndLogVisibility*; 
		PROCEDURE ^ ToggleLabels; 
		PROCEDURE ^ ToggleWrap; 
		PROCEDURE ^ TextChanged(sender, data: ANY); 
		PROCEDURE ^ CursorChanged; 
		PROCEDURE ^ HandleShortcut(ucs: SIZE; flags: SET; keysym: SIZE): BOOLEAN; 
		PROCEDURE ^ Finalize*; 
	END PETPanel; 

	KillerMsg = OBJECT 
	END KillerMsg; 

	BrowseEntry = POINTER TO RECORD 
		prev, next: BrowseEntry; 
		filename: Filename; 
		pos: SIZE; 
	END; 

	Window = OBJECT {EXCLUSIVE} (WMComponents.FormWindow)
	VAR 
		filenameEdit, optionsEdit, positionEdit: WMEditors.Editor; 
		loadBtn, storeBtn, closeBtn, compileBtn, findPCBtn, undoBtn, redoBtn: WMStandardComponents.Button; 
		splitBtn, formatBtn, searchBtn, labelsBtn, wrapBtn, errListBtn: WMStandardComponents.Button; 
		popup: WMPopups.Popup; 
		tabs: WMTabComponents.Tabs; 
		pages: ARRAY MaxNbrOfTabs OF PETPanel; 
		tabList: ARRAY MaxNbrOfTabs OF WMTabComponents.Tab; 
		currentPage: PETPanel; 
		currentPageNr: SIGNED32; 
		page: WMStandardComponents.Panel; 
		codecFormat: ARRAY 128 OF UCS32.CharJQ; 
		autoCodecFormat: ARRAY 128 OF UCS32.CharJQ; 
		projectText: Texts.Text; 
		projectTextFilename: Filename; 
		projectTextModified: BOOLEAN; 
		showTypeHierarchy, showImportedModules: BOOLEAN; 
		windowInfo: WMWindowManager.WindowInfo; 
		currentIcon: WMGraphics.Image; 
		iconIdle, iconWorking: WMGraphics.Image; 
		modifierFlags: SET; 
		browseBase, browseTOS: BrowseEntry; 

		PROCEDURE ^  & New*(c: WMRestorable.Context); 
		PROCEDURE ^ CreateForm(): WMComponents.VisualComponent; 
		PROCEDURE ^ ButtonHandler(sender, data: ANY); 
		PROCEDURE ^ NrUpdatesChanged(nrUndos, nrRedos: SIZE); 
		PROCEDURE ^ InitCodecs; 
		PROCEDURE ^ SelectNextTab; 
		PROCEDURE ^ SelectPreviousTab; 
		PROCEDURE ^ SelectTab(tabNr: SIZE); 
		PROCEDURE ^ RecordCurrentPos; 
		PROCEDURE ^ GotoFileInternal*(CONST filename: ARRAY OF UCS32.CharJQ; pos: SIZE); 
		PROCEDURE ^ GotoFile(CONST filename: ARRAY OF UCS32.CharJQ; pos: SIZE); 
		PROCEDURE ^ GotoDefinition(info: PETTrees.ExternalDefinitionInfo); 
		PROCEDURE ^ GetNrFromPage(page: PETPanel): SIGNED32; 
		PROCEDURE ^ TabSelected(sender, data: ANY); 
		PROCEDURE ^ UpdatePages; 
		PROCEDURE ^ UpdateInfo; 
		PROCEDURE ^ HandleDocumentInfo(CONST info: WMWindowManager.DocumentInfo; new: BOOLEAN; VAR res: INTEGER); 
		PROCEDURE ^ UpdateState; 
		PROCEDURE ^ DragDroppedHandler(x, y: SIZE; dragInfo: WMWindowManager.DragInfo; VAR handled: BOOLEAN); 
		PROCEDURE ^ PositionHandler(sender, data: ANY); 
		PROCEDURE ^ FormatHandler(x, y: SIZE; keys: SET; VAR handled: BOOLEAN); 
		PROCEDURE ^ SetFormatCaption(CONST format: ARRAY OF UCS32.CharJQ); 
		PROCEDURE ^ SetCursorPosition(position: SIGNED32); 
		PROCEDURE ^ SetModified(modified: BOOLEAN); 
		PROCEDURE ^ SetIcon*(icon: WMGraphics.Image); 
		PROCEDURE ^ FormatPopupHandler(sender, data: ANY); 
		PROCEDURE ^ FilenameEditEscapeHandler(sernder, data: ANY); 
		PROCEDURE ^ LoadHandler(sender, data: ANY); 
		PROCEDURE ^ Load(CONST filename, format: ARRAY OF UCS32.CharJQ); 
		PROCEDURE ^ StoreHandler(sender, data: ANY); 
		PROCEDURE ^ CompleteHandler(sender, data: ANY); 
		PROCEDURE ^ Store(CONST filename, format: ARRAY OF UCS32.CharJQ); 
		PROCEDURE ^ NewTab; 
		PROCEDURE ^ HasModifiedPage(): BOOLEAN; 
		PROCEDURE ^ CloseAllTabs*(): BOOLEAN; 
		PROCEDURE ^ CloseHandler*(sender, data: ANY); 
		PROCEDURE ^ SplitHandler(sender, data: ANY); 
		PROCEDURE ^ LinkClickedHandler(sender, data: ANY); 
		PROCEDURE ^ FindPCByString(pcStr: StringsUCS32.String); 
		PROCEDURE ^ FindPC(sender, data: ANY); 
		PROCEDURE ^ UnloadModule; 
		PROCEDURE ^ ВыгрузиМодульИЗависимые; 
		PROCEDURE ^ Close*; 
		PROCEDURE ^ HandleShortcut(ucs: SIZE; flags: SET; keysym: SIZE): BOOLEAN; 
		PROCEDURE ^ Handle*(VAR m: WMMessages.Message); 
	END Window; 
VAR 
	DefaultCompilerName, DefaultCompilerCaption, DefaultCompilerOptions, DefaultCompilerLoadModule, DefaultCompilerFileExtension, DefaultDiffCommand, DefaultDiffPrefix, DefaultDiffSuffix, BackupOnCompileFilename, ScratchTextFilename, StateFileExtension, WindowTitle, DefaultTextFormat: UCS32.PStringJQ; 
	nofWindows: SIGNED32; 
	scratchText: Texts.Text; 
	scratchModified: BOOLEAN; 
	settings: Settings; 
	windowForOpening: Window; 
	StrScratchPanel, StrPETPanel: StringsUCS32.String; 

	PROCEDURE ^ Open*(context: Commands.Context); 
	PROCEDURE ^ Restore*(context: WMRestorable.Context); 
	PROCEDURE ^ CommentSelection*(text: Texts.Text; from, to: Texts.TextPosition); 
	PROCEDURE ^ UncommentSelection*(text: Texts.Text; from, to: Texts.TextPosition); 
	PROCEDURE ^ Comment*; 
	PROCEDURE ^ Uncomment*; 
	PROCEDURE ^ GetSyntaxHighlighterName*(fullname: UCS32.StringJQ; VAR name: UCS32.StringJQ); 
	PROCEDURE ^ ContainsFileExtension(filename, extension: UCS32.StringJQ): BOOLEAN; 
	PROCEDURE ^ ScratchModified(sender, data: ANY); 
	PROCEDURE ^ StoreScratchText; 
	PROCEDURE ^ LoadScratchText; 
	PROCEDURE ^ StoreText(CONST filename: ARRAY OF UCS32.CharJQ; text: Texts.Text); 
	PROCEDURE ^ LoadText(CONST filename: ARRAY OF UCS32.CharJQ): Texts.Text; 
	PROCEDURE ^ IncCount(w: Window); 
	PROCEDURE ^ DecCount(w: Window); 
	PROCEDURE ^ Cleanup; 
	PROCEDURE ^ LoadModule(CONST moduleName: ARRAY OF UCS32.CharJQ); 
	PROCEDURE ^ InitStrings; 
	PROCEDURE ^ ReadLastSelection(): StringsUCS32.String; 
	PROCEDURE ^ RefreshWorkOfPETPanelProc(petPanel: WMComponents.VisualComponent): TS.Module; 
BEGIN
END TFPETUCS32.
