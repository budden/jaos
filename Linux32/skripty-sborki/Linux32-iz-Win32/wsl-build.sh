set -xeuo pipefail 

arg_avto="0"

for ((i=1; i <= "$#"; ++i))
do
    if [ "${!i}" == "--avto" ]; then
        ((i=i+1))	
        arg_avto=${!i}
        continue  
    fi
   
    echo "Неизвестный аргумент: ${!i}";
    exit;
done    

SCRIPT_PATH=$(cd $(dirname $0) && pwd);
# Нормализируем путь, чтобы в нём не было .. 
export JAOS_ROOT=`readlink -f $SCRIPT_PATH/../../..`

cd $JAOS_ROOT
bash Linux32/skripty-sborki/Linux32-iz-Win32/wsl-build-ehtap-1.sh
bash Linux32/skripty-sborki/Linux32-iz-Win32/wsl-build-ehtap-2.sh --avto ${arg_avto}
bash Linux32/skripty-sborki/Linux32-iz-Win32/wsl-build-ehtap-3.sh
