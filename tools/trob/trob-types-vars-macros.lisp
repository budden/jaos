; -*- Encoding: utf-8; system  :trob -*- 

(named-readtables:in-readtable :buddens-readtable-a)
(in-package :trob)



; (TROB:LEXEM :POS 16 :NE-OTKL-PREPROC NIL :STRING "#")

(defstruct lexem
  (:pos (budden-tools:mandatory-slot 'pos) :type integer)
  (:ne-otkl-preproc T :type (or null t))
  (:string (budden-tools:mandatory-slot 'string) :type string))


(defmethod make-load-form ((self lexem) &optional environment)
  (make-load-form-saving-slots self :environment environment))

