; -*- Encoding: utf-8; system  :trob -*- 

(named-readtables:in-readtable :buddens-readtable-a)

(in-package :trob)

(declaim (optimize debug))

(defparameter *директория-яос* #P"C:/ob/jaos/")

(defun dump-lexem (l o)
  (format o "~A" (lexem-string l)))

(defun merge-files (destination &rest pathnames)
  (perga-implementation:perga
   (let sources nil)
   (dolist (pathname pathnames)
     (format t "~&Читаю ~S~%" pathname)
     (with-open-file (in pathname :direction :input)
       (push (read in) sources)))
   (let output-source
     (merge-in-memory sources))
   (with-open-file (ou destination :direction :output
                                :if-exists :supersede
                                :if-does-not-exist :create)
     (mapc (lambda (l) (dump-lexem l ou)) output-source))
   (values)))


(defun canonicalize-lexem (l)
  (format nil "~A~A" 
          (if (lexem-ne-otkl-preproc l) "1" "0")
          (lexem-string l)))

(defun compare-and-canon-lexems (l1 l2)
  (string= (canonicalize-lexem l1)
           (canonicalize-lexem l2)))

(defun merge-in-memory (sources)
  (perga
   (let ss (copy-list sources))
   (let res nil)
   (loop
     (let cur (mapcar 'car ss))
     (unless (first cur)
       (return (nreverse res)))
     (dotimes (i (length sources))
       (pop (elt ss i)))
     ;(print cur)
     (let different-variants
       (DEFPACKAGE-BUDDEN:GROUP-SIMILAR-ITEMS
        cur
        :test 'compare-and-canon-lexems))
     (case (length different-variants)
       (0 (error "WTF?"))
       (1 (push (first (first different-variants)) res))
       (2 
        (let good
          (find-if #'lexem-ne-otkl-preproc different-variants :key 'first))
        (cond
         (good
          (push (first good) res))
         (t
          (push `(inactive-block-mismatch ,@different-variants) res))))
       (t
        (push `(active-block-mismatch ,@different-variants) res))))))

(defun дай-начало-словаря ()
  (perga
   (let начало-словаря-одной-строкой
     (read-file-into-string (trob::str++ *директория-яос* "tools/trob/НачалоСловаря.XML")))
   (let разбитое-по-строкам (split-sequence:split-sequence #\NewLine начало-словаря-одной-строкой))
   (let пустых-строк-в-конце 
     (count-if (lambda (x) (equal (string-right-trim " " x) "")) разбитое-по-строкам :from-end t))
   (let без-закрывающего-тега 
     (subseq разбитое-по-строкам 0 (- (length разбитое-по-строкам) пустых-строк-в-конце -1 -1)))
   (let и-снова-одной-строкой
    (with-output-to-string (вых)
      (dolist (ч без-закрывающего-тега)
        (format вых "~A~%" ч))))
   и-снова-одной-строкой))

(defun слей-файлы-переводов ()
  (perga
   ; все файлы .перев.XML читаем по одному как строки
   (let директория-исходников
     (budden-tools:str++ *директория-яос* "source"))
   (let имена-и-строки-файлов
     (budden-tools:map-dir 
      #'budden-tools:read-file-into-string
      директория-исходников
      :file-test 
      (lambda (f)
        (alexandria:ends-with-subseq "перев.XML" (namestring f)))
      :subdirs :skip))
   (setq имена-и-строки-файлов 
         (sort имена-и-строки-файлов
               'string<
               :key (lambda (x) (namestring (car x)))
               ))
   (let начало-словаря (дай-начало-словаря))


; TODO - грепать имена псевдонимов
; TODO - генерировать отдельный статический и динамический словари



   (:@ with-open-file (вых (str++ *директория-яос* "source/ПереводыЭлементовКода.XML")
                             :direction :output
                             :if-does-not-exist :create
                             :if-exists :supersede))
   (format вых "~A" начало-словаря)
   (dolist (пара имена-и-строки-файлов)
     (let имя-файла (car пара))
     (let файл-как-строка (cdr пара))
     (let отступ (str++ #\Tab))
     (let файл-с-отступами
       (str++ отступ
              (budden-tools:search-and-replace-seq 
               'string
               файл-как-строка
               (str++ #\Newline)
               (str++ #\Newline отступ)
               :all t)))
     (format вых "<!-- ~A -->~%~A" 
             имя-файла файл-с-отступами))
   (format вых "~&</PerevodyEhlementovKoda>~%")
   ))


#|   
   потом подставляем заголовок, который живёт прямо здесь в виде 
   строкового литерала, и генерируем файл переводов.

   Читаем этот файл как XML и сравниваем популярные псевдонимы с известными.
   Результат сравнения печатаем, дальше надо руками поправить. 

 |#


(defun собери-все-переводы-файлов (корень конфигурации)
  (perga
   (let все-имена-файлов-с-повторами nil)
   (dolist (к конфигурации)
     (let к-дир (str++ корень "perevod/" к "/ru"))
     (assert (probe-file к-дир))
     (budden-tools:map-dir
      (lambda (x) 
        (push (namestring (budden-tools:name-and-type-of-a-file x)) все-имена-файлов-с-повторами))
      к-дир
      :subdirs :skip))
   (remove-duplicates все-имена-файлов-с-повторами :test 'string=)))

(defun слей-переводы-файлов (корень конфигурации &key только-показать-план)
  "Корневая директория ЯОС должна заканчиваться на /; конфигурации - это поддиректории. Результат заливается прямо в source"
  (perga
   (let все-переводы-файлов (собери-все-переводы-файлов корень конфигурации))
   (flet задание-для-исходного-файла (fn)
     (let что-будем-сливать nil)
     (dolist (к конфигурации)
       (let имя-файла (str++ корень "perevod/" к "/ru/"
                             (budden-tools:name-and-type-of-a-file fn)))
       (print имя-файла)
       (when (probe-file имя-файла)
         (push имя-файла что-будем-сливать)))
     (when что-будем-сливать
       (let куда (str++ корень "source/" fn))
       `(,куда ,@что-будем-сливать)))
   (dolist (ф все-переводы-файлов)
     (let задание (задание-для-исходного-файла ф))
     (if только-показать-план
         (print задание)
         (apply #'merge-files задание)))))




#|
(progn
  (trob:merge-files #P"C:/ob/jaos/win32/work/РуТестЛиб.ярм" #P"C:/ob/jaos/perevod/nano1/ru/РуТестЛиб.ярм" #P"C:/ob/jaos/perevod/nano2/ru/РуТестЛиб.ярм")
  (trob:merge-files #P"C:/ob/jaos/win32/work/РуТест.ярм" #P"C:/ob/jaos/perevod/nano1/ru/РуТест.ярм" #P"C:/ob/jaos/perevod/nano2/ru/РуТест.ярм"))

(trob:merge-files #P"C:/ob/jaos/win32/work/ПошаговыйОтладчик.ярм" #P"C:/ob/jaos/perevod/nejjtralqnye/ПошаговыйОтладчик.ярм")
|#
