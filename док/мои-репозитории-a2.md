https://gitlab.com/budden/jaos - основной репозиторий для развития, тянет ветку master из a2os-since-2008, для развития будут отдельные ветки.

https://gitlab.com/budden/a2os-since-2008 - репозиторий с полной историей репозитория A2 с 2008 года. Он нужен по той причине, что некоторые важные инструменты сегодня не работают. Имея под рукой полную историю, нетрудно установить время, когда они работали, и чуть легче починить их. В качестве напоминалки самому себе - установка subgit: 

```
# debian 9.4
# Скачиваем subgit откуда-то отсюда: https://subgit.com/download
sudo dpkg --install subgit_3.3.7_all.deb 
```

И команда для импорта.
```
subgit import --username infsvn.anonymous --password anonymous https://svn-dept.inf.ethz.ch/svn/lecturers/a2 /y/svn2git/a2os
cd /y/svn2git/a2os
git push --all origin
```

subgit import позволяет не только создать, но и докачивать изменения из SVN, что происходит намнооого быстрее. Для ориентации по релизам нужна команда
```
svn log --username infsvn.anonymous --password anonymous https://svn-dept.inf.ethz.ch/svn/lecturers/a2 /y/svn2git/a2os
```

Эта команда выполняется очень долго, поэтому журнал на 2020-04-17 сохранён на http://программирование-по-русски.рф/static/svn_a2os_log.7z

https://gitlab.com/budden/ch115/commits/ch115 - Основная ветка ch115 ответвлена от версии от 2018-11-06 - некая работа над шрифтами, скорее всего, не увенчалась большим успехом. Также есть версия ч_115, ответвлена от версии 2016-01-05, ради отладчика, никаких работ не проводилось. 
Ветка master не содержит моих изменений. 
 
https://github.com/budden/metacore-a2os-tiny-patch - форк версии Ярослава, ответвлена от швейцарской версии в районе апреля 2019, проведена некая работа над шрифтами, результат неясен.

