set -xeuo pipefail 

SCRIPT_PATH=$(cd $(dirname $0) && pwd);
export JAOS_ROOT=`readlink -f $SCRIPT_PATH`

win32Dir="$JAOS_ROOT/Win32"
testDir="$JAOS_ROOT/Win32/work/test"

mkdir -p $testDir
cd $testDir

git clean -x -d -f .
# git checkout -f .

peremesti_diff() {
 Tek=$1
 Pred="${1}.Pred"
 if [ -f $Pred ]; then
  rm $Pred; fi
 if [ -f $Tek ]; then
  mv $Tek $Pred; fi 
}

peremesti_diff Oberon.Compilation.Test.Diff
peremesti_diff Oberon.Execution.Test.Diff

cd $win32Dir
cmd.exe /c a2-testy.bat

cd $testDir
grep "failed tests" FoxCompilationTest.Log
grep "failed tests" FoxExecutionTest.Log

# tail FoxExecutionTest.Log
